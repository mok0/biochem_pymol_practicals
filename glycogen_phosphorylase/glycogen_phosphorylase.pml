# Glycogen phosphorylase T and R states.
# Copyright (C) 2015-2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Glycogen Phosphorylase
# Version 2.1

python
def load_from_local_or_repo(fname, objnam):
    import posixpath
    bb = "https://bitbucket.org/mok0/biochem_pymol_practicals/raw/master/"
    localpath = os.path.join("pdb", fname)
    repopath =  posixpath.join(bb, "glycogen_phosphorylase", "pdb", fname)
    if os.path.exists(fname):
        cmd.load(fname, objnam)
    elif os.path.exists(localpath):
        cmd.load(localpath, objnam)
    else:
        cmd.load(repopath, objnam)
python end

reinitialize

# Change default values
set dash_color, white

fetch 1gpa, async=0

# Load local copy if we have one
load_from_local_or_repo('8gpb.pdb1.gz', '8gpb')

hide everything

color orange, /8gpb//A
color gold, /8gpb//B

# Assemble chains A and B in one object:
create T_chains, /8gpb//A+B
show cartoon, T_chains

# create PLP site
# PLP site. PLP is 999, sulphate is 901.
create T_PLP, /T_chains///901+999+680 and not (name C or name O or name N)
show sticks, T_PLP
util.cnc('T_PLP')

create T_sc, /T_chains///90+491+574 and (sidechain or name CA)
show sticks, T_sc
util.cnc('T_sc')

# Phosphorylation site:
create T_Ser14, /T_chains///14+43+69 and (sidechain or name CA)
show sticks, T_Ser14
color nitrogen, /T_Ser14 and name N*
color oxygen, /T_Ser14 and name O*

group T_state, T_chains T_PLP T_sc T_Ser14, add

# Create the R state object (from structure 1gpa)

create R_chains, /1gpa//A+B
# beta strand 89-91 disturbs our view of PLP binding site
alter  /R_chains//B/89-91, ss='L'
show cartoon, R_chains
color marine, /R_chains//A
color slate, /R_chains//B

# Align the T and R state structures
align R_chains, T_chains

# PLP site. PLP is 999, sulphate is 901.
create R_PLP, /R_chains///901+999+680 and not (name C or name O or name N)
show sticks, R_PLP
color nitrogen, /R_PLP and name N*
color oxygen, /R_PLP and name O*

# Hide main chain atoms of 491+574
create R_sc, /R_chains///90+491+574 and (sidechain or name CA)
show sticks, R_sc
util.cnc('R_sc')

# Phosphorylation site:
create R_Ser14, /R_chains///14+43+69 and (sidechain or name CA or name P)
show sticks, R_Ser14
color nitrogen, /R_Ser14 and name N*
color oxygen, /R_Ser14 and name O*
color phosphorus, /R_Ser14 and name P*

# Recolor PLP and SO4
select resn PLP or resn SO4
util.cbaw('sele')
delete sele
delete 8gpb
delete 1gpa

# Hbonds between phosphorylated Ser-14 and lysines
distance R_Hb, /R_Ser14//B/SEP`14/O2P, /R_Ser14//A/ARG`43/NH1
distance R_Hb, /R_Ser14//B/SEP`14/O2P, /R_Ser14//A/ARG`43/NH2
distance R_Hb, /R_Ser14//B/SEP`14/O3P, /R_Ser14//B/ARG`69/NH1
distance R_Hb, /R_Ser14//B/SEP`14/O1P, /R_Ser14//B/ARG`69/NH2

distance R_Hb, /R_Ser14//A/SEP`14/O2P, /R_Ser14//B/ARG`43/NH1
distance R_Hb, /R_Ser14//A/SEP`14/O2P, /R_Ser14//B/ARG`43/NH2
distance R_Hb, /R_Ser14//A/SEP`14/O3P, /R_Ser14//A/ARG`69/NH1
distance R_Hb, /R_Ser14//A/SEP`14/O1P, /R_Ser14//A/ARG`69/NH2


# Hbonds to sulphate
distance R_Hb, /R_PLP//A/SO4`901/O1, /R_sc//A/LYS`574/NZ
distance R_Hb, /R_PLP//A/SO4`901/O3, /R_sc//A/LYS`574/NZ

distance R_Hb, /R_PLP//B/SO4`901/O1, /R_sc//B/LYS`574/NZ
distance R_Hb, /R_PLP//B/SO4`901/O3, /R_sc//B/LYS`574/NZ

hide labels, R_Hb

group R_state, R_chains R_PLP R_Ser14 R_sc R_Hb, add

### Scene F1
enable T_state
disable R_state

set_view (\
    -0.663246870,   -0.248700723,    0.705870986,\
     0.690303922,    0.161065564,    0.705368817,\
    -0.289117366,    0.955093503,    0.064853095,\
     0.000000000,    0.000000000, -386.597900391,\
     6.047928810,    6.092499256,   58.431209564,\
   249.636505127,  523.559265137,  20.)

 scene F1,  store


### Scene F2
enable R_state
disable T_state

scene F2, store

### Scene F3
enable R_state
disable T_state

set_view (\
    -0.366254389,    0.002466233,   -0.930512547,\
    -0.709298253,    0.646528542,    0.280895978,\
     0.602293015,    0.762886345,   -0.235045388,\
     0.000000000,    0.000000000,  -58.395702362,\
    -5.686999798,   17.406000137,   60.874000549,\
    35.417362213,   81.374046326,  20.0 )

scene F3, store

### Scene F4
enable T_state
disable R_state
scene F4, store

### Scene F5 ###
enable R_state
disable T_state

set_view (\
    -0.478953838,   -0.569911420,    0.667688429,\
    -0.497970670,    0.802773833,    0.328005433,\
    -0.722934306,   -0.175390139,   -0.668286026,\
    -0.000228971,    0.000153499,  -65.107437134,\
    22.698280334,   24.065427780,   90.053985596,\
    55.539237976,   74.672042847,  20.0 )
scene F5, store

### Scene F6 ###
enable T_state
disable R_state
scene F6, store

scene F1, recall
