SHELL = /bin/sh

SUBDIRS = atcase carbamoyl-phosphate-synthetase fas \
glycogen_phosphorylase hexokinase include potassium_ion_channel \
proteases proteasome ribonucleotide-reductase rnase_a serca \
tryptophan-synthase methylmalonyl-coa-mutase aspartate-aminotransferase

.PHONY: subdirs $(SUBDIRS) clean

subdirs: $(SUBDIRS)

$(SUBDIRS):
	@ $(MAKE) -C $@

clean:
	@rm -f *~ *.cif *.pdb
	for d in $(SUBDIRS); do echo $$d; $(MAKE) --directory=$$d clean; done
