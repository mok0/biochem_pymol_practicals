# Copyright (C) 2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Illustrate induced fit of hexokinase.
# Version 1.1

reinitialize

# Change default values
set dash_color, white

# Open form
fetch 1ig8, async=0

# glucose-bound form
fetch 1bdg, async=0

hide everything

# Superimpose the closed onto the 1ig8 structure
align 1bdg, 1ig8

# Open structure, entry 1ig8 #

color limon, 1ig8
show cartoon, 1ig8

# Residue numbers are different in open and closed structures:
create pocket_o, /1ig8///86+90+158+176+211+237+269+302 and (not name C or name N or name O)
show sticks, pocket_o
util.cnc('pocket_o')

group Open, 1ig8 pocket_o

# Closed structure, entry 1bdg #

color cyan, 1bdg
show cartoon, 1bdg

# Glucose
create glc, /1bdg///501+502
show sticks, glc
util.cbaw('glc')

# Water molecules interacting with GLC
create hoh, /1bdg//A/630+652+933
show nb_spheres, hoh
color oxygen, hoh

# Glucose and sulphate binding pocket
create pocket_c, /1bdg///84+88+155+172+173+209+235+260+294 and not (name C or name O or name N)
show sticks, pocket_c
util.cnc('pocket_c')

# Hydrogen bonds between GLC and protein

dist Hbond, /glc///501/O2, /1bdg///260/OE2
dist Hbond, /glc///501/O2, /1bdg///172/OG1
dist Hbond, /glc///501/O6, /1bdg///173/NZ
dist Hbond, /glc///501/O4, /1bdg///209/OD1
dist Hbond, /glc///501/O3, /1bdg///260/OE1
dist Hbond, /glc///501/O1, /1bdg///294/OE2
dist Hbond, /glc///501/O4, /1bdg///235/ND2
dist Hbond, /glc///501/O5, /1bdg///173/NZ

# Protein-SO4 binding
dist Hbond, /glc///502/O4, /1bdg///88/OG1

# H-bonds to water molecules
dist Hbond, /glc///502/O4, /hoh///652/O
dist Hbond, /glc///501/O6, /hoh///652/O
dist Hbond, /glc///502/O3, /hoh///630/O
dist Hbond, /glc///502/O2, /hoh///630/O
dist Hbond, /hoh///630/O, /hoh///933/O
dist Hbond, /hoh///933/O, /1bdg///155/OG
dist Hbond, /hoh///933/O, /1bdg///84/OD1
hide labels, Hbond

group Closed, 1bdg glc pocket_c hoh Hbond

# View F1: Overview of the open form of hexokinase

set_view (\
-0.986116290,    0.163739830,   -0.027341211,\
 0.144425750,    0.927417934,    0.345008671,\
 0.081854001,    0.336274445,   -0.938194633,\
 0.000293560,   -0.000456065, -264.690521240,\
 30.684375763,   87.266510010,    0.783107758,\
 238.896911621,  290.462493896,   20.0 )

disable Closed
disable pocket_o
scene F1, store


disable Open
enable Closed
disable hoh pocket_c Hbond

# View F2: Overview of the closed form of hexokinase

set_view (\
-0.986116290,    0.163739830,   -0.027341211,\
 0.144425750,    0.927417934,    0.345008671,\
 0.081854001,    0.336274445,   -0.938194633,\
 0.000293560,   -0.000456065, -264.690521240,\
 30.684375763,   87.266510010,    0.783107758,\
 238.896911621,  290.462493896,   20.0 )

scene F2, store

# View F3: Detail of glucose bound, including interacting side chains,
# solvent and hydrogen bonds

set_view (\
    -0.146265596,   -0.062346503,   -0.987264752,\
    -0.387550324,    0.921836913,   -0.000804080,\
     0.910154462,    0.382502347,   -0.158993885,\
     0.000000000,    0.000000000,  -81.466964722,\
    27.549669266,   82.481422424,    0.530912876,\
          67, 93, 20)

enable hoh pocket_c Hbond
scene F3, store

# View F4 of the empty active site

set_view (\
    -0.146265596,   -0.062346503,   -0.987264752,\
    -0.387550324,    0.921836913,   -0.000804080,\
     0.910154462,    0.382502347,   -0.158993885,\
     0.000000000,    0.000000000,  -81.466964722,\
    27.549669266,   82.481422424,    0.530912876,\
          63, 98, 20)

disable Closed
enable Open pocket_o

scene F4, store

scene F1, recall
