# Copyright (C) 2015-2017 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Coronavirus protease
# Version 2.3
python
def load_from_local_or_repo(fname, objnam):
    import posixpath
    bb = "https://bitbucket.org/mok0/biochem_pymol_practicals/raw/master/"
    localpath = os.path.join("pdb", fname)
    repopath =  posixpath.join(bb, "proteases", "pdb", fname)
    if os.path.exists(fname):
        cmd.load(fname, objnam)
    elif os.path.exists(localpath):
        cmd.load(localpath, objnam)
    else:
        cmd.load(repopath, objnam)
python end

reinitialize

fetch 1p9s, async=0
fetch 1p9uA, async=0
# Can't use fetch because 1p9t is a model, not an experimental
# structure
load_from_local_or_repo('1p9t.pdb.gz', '1p9t')
hide everything

align 1p9t, 1p9s
align 1p9uA, 1p9s

color limon, 1p9uA
color hotpink, 1p9t

color cyan, /1p9s//A
color marine, /1p9s//B

show ribbon, 1p9s
show ribbon, 1p9u
show ribbon, 1p9t

set_view (\
    -0.974637687,   -0.179683179,    0.133398980,\
    -0.193913192,    0.975633860,   -0.102625318,\
    -0.111708455,   -0.125890359,   -0.985734999,\
     0.000024930,   -0.000001550, -217.396697998,\
    29.271383286,   -0.835785151,   15.730168343,\
   171.397796631,  263.397094727,  -20.0 )
