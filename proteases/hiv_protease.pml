# Copyright (C) 2015-2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# HIV Protease
# Version 1.1

reinitialize

# 1hsi, 2hpf, 1hsg
fetch 1hsi, async=0
fetch 2hpf, async=0
fetch 1hsg, async=0

align 2hpf, 1hsi
align 1hsg, 1hsi

hide everything

# The secondary structure assignments are different in the three
# structures, and that doesn't look good. Set them manually.

alter /1hsg///1-98, ss='L'
alter /1hsg///1-5, ss='S'
alter /1hsg///9-15, ss='S'
alter /1hsg///18-25, ss='S'
alter /1hsg///30-33, ss='S'
alter /1hsg///35-38, ss='S'
alter /1hsg///42-49, ss='S'
alter /1hsg///52-59, ss='S'
alter /1hsg///62-66, ss='S'
alter /1hsg///69-73, ss='S'
alter /1hsg///75-78, ss='S'
alter /1hsg///83-86, ss='S'
alter /1hsg///87-92, ss='H'
alter /1hsg///94-98, ss='S'

alter /2hpf///1-98, ss='L'
alter /2hpf//A+B/1-5, ss='S'
alter /2hpf///9-15, ss='S'
alter /2hpf///18-25, ss='S'
alter /2hpf///30-33, ss='S'
alter /2hpf///35-38, ss='S'
alter /2hpf///42-49, ss='S'
alter /2hpf///52-59, ss='S'
alter /2hpf///62-66, ss='S'
alter /2hpf///69-73, ss='S'
alter /2hpf///75-78, ss='S'
alter /2hpf///83-86, ss='S'
alter /2hpf///87-92, ss='H'
alter /2hpf///94-98, ss='S'

alter /1hsi///1-98, ss='L'
alter /1hsi///1-5, ss='S'
alter /1hsi///9-15, ss='S'
alter /1hsi///18-25, ss='S'
alter /1hsi///30-33, ss='S'
alter /1hsi///35-38, ss='S'
alter /1hsi///42-49, ss='S'
alter /1hsi///52-59, ss='S'
alter /1hsi///62-66, ss='S'
alter /1hsi///69-73, ss='S'
alter /1hsi///75-78, ss='S'
alter /1hsi///83-86, ss='S'
alter /1hsi///87-92, ss='H'
alter /1hsi///94-98, ss='S'

rebuild

# 1hsi
show cartoon, 1hsi
color green, 1hsi

# 2hpf
show cartoon, /2hpf//A+B
color cyan, 2hpf
create Peptide, /2hpf//S
show sticks, Peptide
util.cbay('Peptide')

# 1hsg
show cartoon, 1hsg
color orange, 1hsg
create Crixivan, /1hsg///902
show sticks, (Crixivan)
util.cbay('Crixivan')

# Select active site in all 3 structures
select 25/
show sticks, (sele)
util.cbay('(sele)')

# F1 #

disable 2hpf
disable Peptide
disable 1hsg
disable Crixivan

set_view (\
    -0.144101113,   -0.929484844,    0.339551300,\
     0.432752937,    0.249389485,    0.866330504,\
    -0.889919579,    0.271781564,    0.366300642,\
    -0.000009298,   -0.000018522, -136.351257324,\
    13.347823143,   21.475545883,   16.219470978,\
   113.334999084,  159.368347168,   20.0 )

scene F1, store


# F2 #

disable 1hsi
enable 2hpf
enable Peptide

scene F2, store

# F3 #

disable 2hpf
disable Peptide
enable 1hsg
enable Crixivan

scene F3, store

scene F1, recall
