# Copyright (C) 2015-2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Subtilisin
# Version 2.1

reinitialize

# Change default values
set dash_color, white

fetch 1cse, async=0
hide everything

# 1-154 marine
color marine, resi 1-154

# 155-239 lime
color limon, resi 155-239

# 240-275 dark green
color forest, resi 240-275

# Chain I (eglin) raspberry
color raspberry, chain I

create Eglin, chain I
hide Eglin

create Subtilisin, chain E and resi 1-275
show cartoon, Subtilisin

# See http://www.pymolwiki.org/index.php/Cartoon_side_chain_helper
set cartoon_side_chain_helper, on

# Triad residues
create Triad, chain E and resi 32+64+221 and (sidechain or name CA)
util.cbag Triad
show sticks, Triad

### Hydrogen bonds
dist Asp-His, /Triad//E/ASP`32/OD2, /Triad//E/HIS`64/ND1, 3.2
dist His-Ser, /Triad//E/HIS`64/NE2, /Triad//E/SER`221/OG, 3.2
show dashes, Asp-His
show dashes, His-Ser
hide labels, Asp-His
hide labels, His-Ser

# Asn 155
create Asn155, chain E and resi 155 and (sidechain or name CA)
util.cbay Asn155
show sticks, Asn155

set_view (\
     0.634457529,    0.749718547,   -0.188111678,\
     0.681436241,   -0.427647799,    0.593936503,\
     0.364838302,   -0.505013824,   -0.782212138,\
     0.000000000,    0.000000000, -131.733078003,\
    35.950000763,  -27.339000702,   11.088999748,\
   108.837539673,  154.628585815,  20.0 )

scene F1, store

set_view (\
     0.342248768,   -0.172350973,   -0.923667312,\
     0.847192407,    0.481744468,    0.224021047,\
     0.406359762,   -0.859197021,    0.310891449,\
     0.000000000,    0.000000000, -131.733078003,\
    35.950000763,  -27.339000702,   11.088999748,\
   108.837539673,  154.628585815,  20.0 )

scene F2, store

color hotpink, chain E and resi 43-95
scene F3, store

# Recolor subtilisin and redraw
color marine, chain E and resi 43-95
show cartoon, Subtilisin

# Eglin: I8-I70
select Eglin and resi 43-47
show stick, (sele)
select Eglin and (resi 8-43 or resid 47-70)
show cartoon, (sele)
select none

set_view (\
     0.342248768,   -0.172350973,   -0.923667312,\
     0.847192407,    0.481744468,    0.224021047,\
     0.406359762,   -0.859197021,    0.310891449,\
     0.000000000,    0.000000000, -200.957565308,\
    45.363998413,  -24.908000946,   -0.467000008,\
   161.394042969,  240.521087646,  20.0 )

scene F4, store

set_view (\
    -0.058849879,   -0.402307093,   -0.913611829,\
     0.851093054,    0.458071202,   -0.256534427,\
     0.521703601,   -0.792667270,    0.315444499,\
     0.000000986,   -0.000000065,  -42.575309753,\
    33.884445190,  -22.058938980,    3.803536177,\
    37.059139252,   48.091480255,  20.0 )

util.cnc('Eglin')
scene F5, store

scene F1, recall
