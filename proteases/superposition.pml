# Copyright (C) 2015-2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Superposition of trypsin, chymotrypsin and subtilisin
# Version 2.2

reinitialize

fetch 2ptc, async=0
fetch 3gch, async=0
fetch 1sbt, async=0

# For some reason, secondary structure is screwed up in trypsin #
cmd.dss("2ptc")

# Align chymotrypsin and subtilisin to trypsin #
super 3gch, 2ptc

# super and cealign have different ordering of the arguments
cealign 2ptc, 1sbt

# Align trypsin and subtilisin on the catalytic triad #
# In trypsin it is: His-75, Asp-102, Ser-195 and in subtilisin
# it is:
# His-64, Asp-32, Ser-221
# Using the simple:
# pair_fit /1sbt///64+32+221, /2ptc//E/57+102+195
# does not give the proper alignment, because the Asp 32/102
# groups have been assigned different rotamers.

pair_fit \
/1sbt///64/ca, /2ptc//E/57/ca, \
/1sbt///64/cb, /2ptc//E/57/cb, \
/1sbt///64/cg, /2ptc//E/57/cg, \
/1sbt///64/nd1, /2ptc//E/57/nd1, \
/1sbt///64/cd2, /2ptc//E/57/cd2, \
/1sbt///64/ce1, /2ptc//E/57/ce1, \
/1sbt///64/ne2, /2ptc//E/57/ne2, \
/1sbt///32/ca, /2ptc//E/102/ca, \
/1sbt///32/cb, /2ptc//E/102/cb, \
/1sbt///32/cg, /2ptc//E/102/cg, \
/1sbt///32/od2, /2ptc//E/102/od1, \
/1sbt///32/od1, /2ptc//E/102/od2, \
/1sbt///221/ca, /2ptc//E/195/ca, \
/1sbt///221/cb, /2ptc//E/195/cb, \
/1sbt///221/og, /2ptc//E/195/og

hide everything

# Draw trypsin and chymotrypsin
extract Trypsin, 2ptc and chain E
color slate, Trypsin
extract Chymotrypsin, 3gch
color limegreen, Chymotrypsin
extract Subtilisin, 1sbt
color warmpink, Subtilisin

# We don't need these objects anymore
delete 2ptc
delete 3gch
delete 1sbt

show cartoon, Trypsin
select /Trypsin and resi 57+102+195
show stick, (sele)

show cartoon, Chymotrypsin
select /Chymotrypsin and resi 57+102+195
show stick, (sele)

# Version 1.7.4.5 Edu does not show this bond
if cmd.get_version()[2] < 1800: \
   cmd.bond('/Chymotrypsin///195/CB', '/Chymotrypsin///195/OG')

set_view (\
   -0.721377552,   -0.692402422,   -0.013891028,\
   -0.161653236,    0.148836032,    0.975560606,\
   -0.673411667,    0.705991030,   -0.219298467,\
    0.000003554,   -0.000001561, -142.116989136,\
   10.150899887,   65.144561768,   19.640140533,\
  108.666404724,  178.367507935,  -20.000000000 )

scene F1, store

# Scene 3, same orientation, switch on subtilisin and
# switch off chymotrypsin

disable Chymotrypsin
show cartoon, Subtilisin
select /Subtilisin and resi 64+32+221
show stick, (sele)
enable Sutilisin
scene F3, store

# Scene 2

disable Subtilisin
enable Chymotrypsin
select Trypsin and chain E and resi 57+102+195
util.cbac (sele)
show sticks, (sele)
select /Chymotrypsin//B/57+102 or /Chymotrypsin//C/195
util.cbag (sele)
show sticks, (sele)
select none
set_view (\
     0.342552006,    0.004408182,    0.939488351,\
    -0.320579827,    0.940519691,    0.112475730,\
    -0.883111358,   -0.339710116,    0.323590398,\
     0.000000000,    0.000000000,  -38.158973694,\
     9.322250366,   73.352256775,   21.048543930,\
    34.718074799,   41.599876404,  -20.000000000 )

scene F2, store

# Scene F4, same orientation, switch off chymotrypsin,
# switch on subtilisin

disable Chymotrypsin
enable Subtilisin

scene F4, store

scene F1, recall
