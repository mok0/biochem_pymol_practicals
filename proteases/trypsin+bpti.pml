# Copyright (C) 20152016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Trypsin with pancreatic trypsin inhibitor
# Version 2.1

reinitialize

# Change default values
set dash_color, white

fetch 2ptc, async=0
hide everything

# Find secondary structure, default is not good
cmd.dss("2ptc")

extract Trypsin, 2ptc and chain E

color raspberry, Trypsin
show cartoon, Trypsin

extract BPTI, 2ptc and chain I
color slate, BPTI

delete 2ptc

# Select parts of inibitor and draw cartoon
select /BPTI///1-13+17-58
show cartoon, (sele)

# Select residues 13-17 and draw with sticks
select /BPTI///13-17
show stick, (sele)

# Select SS-bridge, draw with sticks and color yellow

select chain I and resi 14+38 and (sidechain or name ca)
color yellow, (sele)
color yellow,  /BPTI///14
show stick, (sele)

# Select Lysine 15 and color with green carbons
select /BPTI///15
util.cbag (sele)

set_view (\
     0.853775859,   -0.030787602,   -0.519736350,\
     0.241896749,    0.907421410,    0.343624234,\
     0.461040467,   -0.419098049,    0.782181621,\
    -0.000304345,    0.000229090, -169.858032227,\
    11.875866890,   76.056167603,   15.604863167,\
   135.324935913,  204.378021240,  20.000000000 )

scene F1, store

create Triad, chain E and resi 57+102+195
show sticks, Triad
util.cbay Triad

dist Triad_Hbonds, /Triad///102/OD2, /Triad///57/ND1
dist Triad_Hbonds, /Triad///57/NE2, /Triad///195/OG
hide labels, Triad_Hbonds

set_view (\
     0.214369282,    0.792622507,    0.570789635,\
    -0.297950387,    0.609582424,   -0.734603584,\
    -0.930205584,   -0.012588851,    0.366835952,\
     0.000000000,    0.000000000,  -68.027610779,\
    10.543999672,   78.717002869,   16.653999329,\
    59.933200836,   76.122024536,  20.000000000 )

scene F2, store

hide cartoon, Trypsin
show cartoon, /Trypsin///16-189+195-214+219-245

create Pocket, chain E and resi 189-195+214-219
show sticks, Pocket
util.cbaw Pocket
select resi 414
show nb_sphere, (sele)

dist hbonds, /BPTI///15/NZ, /Pocket///190/OG
dist hbonds, /BPTI///15/NZ, /Trypsin///414/O
dist hbonds, /Trypsin///414/O, /Pocket///189/OD1
dist hbonds, /Trypsin///414/O, /Pocket//E/GLY`219/O

dist hbonds, /BPTI///15/O, /Pocket///193/N
dist hbonds, /BPTI///15/O, /Pocket///194/N
dist hbonds, /BPTI///15/O, /Pocket///195/N

dist hbonds, /BPTI///15/N, /Pocket///195/OG
hide labels, hbonds

# /2ptc//E/HOH`414/O

set_view (\
     0.011043232,   -0.678110242,    0.734882236,\
    -0.713400424,    0.509636879,    0.480976492,\
    -0.700678825,   -0.529573977,   -0.478133321,\
     0.000000000,    0.000000000,  -58.735019684,\
    17.464000702,   71.290000916,   18.617000580,\
    55.679485321,   61.790546417,  20.0 )

scene F3, store

disable Triad-Hbonds

set_view (\
    -0.560010970,   -0.021712229,   -0.828205168,\
    -0.369159669,    0.901474893,    0.225991935,\
     0.741696596,    0.432299584,   -0.512849331,\
     0.000000000,    0.000000000,  -58.735019684,\
    14.397533417,   72.669708252,   16.473264694,\
    52.895805359,   64.574241638,  20.000000000 )

scene F4, store
scene F1, recall
