# Copyright (C) 2015-2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Chymotrypsin
# Version 2.1

reinitialize

# Change default values
set dash_color, white

fetch 5cha, async=0
hide all

## Fixes to secondary structure assignments
alter 28/, ss=''
alter 49/, ss=''
alter 172/, ss=''
alter 80/, ss='S'
alter 163/, ss='S'
alter 202-203/, ss='S'
alter 206-210/, ss='S'

# Domain 1 chain B, 16-146 #

create Domain1, chain B and (resi 16-146) or (chain A and (resi 1-8))
color marine, Domain1
select chain A
color green, (sele)
show cartoon, Domain1

# Domain 2 chain C, 149-245 #
create Domain2, chain C and (resi 149-245)
color hotpink, Domain2
show cartoon, Domain2

## Disulphide bridges ##
create SS_bonds, (cys/ca+cb+sg) and byres (cys/sg and bound_to cys/sg) and (chain A or chain B or chain C)
color yellow, SS_bonds
show sticks, SS_bonds

# Catalytic triad #

create Triad, (chain B and resi 57+102) or (chain C and resi 195)
set cartoon_side_chain_helper, on
show sticks, Triad
color limegreen, Triad

# Mark the missing loop

SPHERE=7.0

obj=[SPHERE, 25.789, 14.616, 38.670,  0.25, \
SPHERE, 27.780, 14.558, 40.408, 0.25, \
SPHERE, 30.376, 13.802, 41.225, 0.25, \
SPHERE, 32.469, 12.619, 40.536, 0.25, \
SPHERE, 33.666, 11.475, 39.248, 0.25, \
SPHERE, 24.666, 13.126, 34.876, 0.25, \
SPHERE, 24.872, 14.062, 36.940, 0.25]

cmd.load_cgo(obj,'loop')
color marine, loop

# Set the views

set_view (\
    -0.835484862,   -0.509382546,   -0.206136703,\
     0.539708018,   -0.831198990,   -0.133493841,\
    -0.103339545,   -0.222786009,    0.969368100,\
     0.000074409,    0.000171348, -138.438095093,\
    32.455989838,   20.157714844,   20.010362625,\
    30.546642303,  246.309753418,  20. )
scene F1, store

set_view (\
    -0.703094602,   -0.454430312,   -0.546945274,\
     0.179878771,   -0.857800961,    0.481475502,\
    -0.687963963,    0.240136638,    0.684856117,\
     0.000074409,    0.000171348, -138.438095093,\
    32.455989838,   20.157714844,   20.010362625,\
    30.546642303,  246.309753418,  20.0 )
scene F2, store

set_view (\
    -0.990670621,   -0.035784323,    0.131489217,\
     0.025768284,   -0.996687651,   -0.077115841,\
     0.133813068,   -0.073009931,    0.988307655,\
     0.000074409,    0.000171348, -138.438095093,\
    32.455989838,   20.157714844,   20.010362625,\
    30.546642303,  246.309753418,  20.0 )
scene F3, store

set_view (\
     0.560755968,   -0.684958994,    0.465168059,\
    -0.400562823,   -0.716112316,   -0.571602404,\
     0.724634051,    0.134198993,   -0.675931990,\
     0.000074409,    0.000171348, -138.438095093,\
    32.455989838,   20.157714844,   20.010362625,\
    30.546642303,  246.309753418,  20.0 )
scene F4, store

# Detailed view of catalytic triad #

util.cbay('Triad')
distance Hbonds, /Triad/B/B/HIS`57/NE2, /Triad/C/C/SER`195/OG
distance Hbonds, /Triad/B/B/HIS`57/ND1, /Triad/B/B/ASP`102/OD1
hide labels, Hbonds
set_view (\
    -0.694107890,   -0.266620308,   -0.668674409,\
     0.553267837,   -0.791853070,   -0.258573711,\
    -0.460548580,   -0.549432993,    0.697141349,\
     0.000000000,    0.000000000,  -35.091056824,\
    28.351253510,   25.123500824,   20.838417053,\
    27.666057587,   42.516056061,   20.000000000 )
scene F5, store

# Show Domains 1 & 2 uperimposed #

# Superimpose these zones:
#  -  Domain 1 24-110
#  -  Domain2 149-235
# We need to make copies of the objects not to screw up views F1-F4.

cmd.copy('Dom1', 'Domain1')
cmd.copy('Dom2', 'Domain2')

align /Dom1///24-110, /Dom2///149-235

for o in ("Domain1", "Domain2", "SS_bonds", "loop", "Triad", "Hbonds"): cmd.disable(o)

set_view (\
    -0.133585364,   -0.990900278,   -0.016495120,\
     0.034339860,    0.012006157,   -0.999338448,\
     0.990442932,   -0.134063318,    0.032423511,\
     0.000000000,    0.000000000, -139.295135498,\
    26.203531265,   20.003187180,   15.917690277,\
   109.821342468,  168.768920898,  20.0 )

scene F6, store

scene F1,recall
