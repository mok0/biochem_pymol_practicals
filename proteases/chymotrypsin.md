<!--- -*- mode: markdown; mode: auto-fill; fill-column: 75; -*- -->

# Proteases
## Chymotrypsin ##

In this excercise we will look at the structure of the protease
_chymotrypsin_, from the PDB entry [5cha][].

Chymotrypsin consists of two β-barrel domains, domain 1 and 2 shown in blue
and red, respectively. The active site contains the socalled "catalytic
triad" consisting of Ser195, His57 and Asp102 at the domain interface, this
is shown with green sticks.

- Identify the two domains of the structure by turning them on or off using
  the buttons _Domain 1_ og _Domain 2_ in the list to the right in the
  PyMol window. Also, identify other elements of the structure by turning
  them on or off.

The two domains are very alike and are arranged in a socalled "pseudo
two-fold symmetry".

- Press F1 on the keyboard to look down the approximate two-fold axis
  between the domains, and F2 or F3 to look down the axis of each
  barrel in turn. What do you think "pseudo two-fold symmetry" means?

When chymotrypsinogen is activated to yield chymotrypsin, the molecule is
cleaved in two places to produce the chains called A, B and C. The peptide
that is cut out between chains B and C is shown with small spheres in the
structure and is not very important for the activation. In contrast, the
cleavage at residue 16 is critical. Find residue 16 and explain why it is
important to the activation of chymotrypsin.

### Answers

1. -

2. "Pseudo-symmetry" is a symmetry that is not perfect. It means that two
   domains with a similar fold, and arranged so a rotation of approximately
   180 degreess would approximately superimpose the two, can be said to
   have pseudo two-fold symmetry. If the symmetry is perfect and the two
   molecules are identical, it would be called a proper two-fold symmetry.

<pre>
     ______________  _____         ______________  _____
     \            / /     \        \            / /     \
      \	         / /       \ 	    \	       / /       \
       \   A    / /    A    \	     \	  A   / /    B    \
        \      / /           \	      \      / /           \
         \----/ /-------------\	       \----/ /-------------\

           Two-fold symmetry          Pseudo two-fold symmetry
</pre>

3. The new amino terminal (N-terminal), formed at residue 16 when the
   peptide bond between residues 15 and 16 is hydrolyzed, moves into the
   active site and becomes part of the so-called "oxyanion pocket".


## Views
### F1
### F2
### F3

[5cha]:
