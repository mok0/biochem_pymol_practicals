<!--- -*- mode: markdown; mode: auto-fill; fill-column: 75; -*- -->

# Factor XIII

Factor XIII is the zymogen of a transglutaminase which is activated by
thrombin in the presence of Ca2+ and fibrin. The protein circulates in
plasma as a tetramer of two A and two B subunits. The catalytic
activity is located in the A subunits. Factor XIIIa crosslinks several
physiological substrates, among them fibrin, factor V, a
2-antiplasmin, several adhesive proteins and cytoskeleton proteins of
several cell types, including platelets and connective
tissue. Therefore factor XIII has a pivotal function in the mechanical
stabilisation of clots as well as in the protection of clots against
premature lysis.

- Coordinates from file: 1GGU.pdb
- Coordinates for actinidin, thrombin, and leupeptin-inhibited papain are from Brookhaven Data Bank  files 2ACT, 2HGT, and 1POP, respectively.

- F1  Factor XIII, active sites and activation peptide.
  Coordinates from PDB entry 1GGU.
- F2  Domain structure of Factor XIII monomer
- F3 Superposition of active site residues of actinidin and thrombin
  to factor XIII. This kinemage shows the similarites and differences
  between the active site residues of factor XIII (pinks), actinidin
  (cyan & white), and thrombin (greens).
- F4 Superposition of conserved regions of factor XIII and actinidin.
  This is a display of the similarities between the transglutaminase
  factor XIII (in pink) and the cysteine protease actinidin (in
  white).  Active sites are in hotpink and cyan respectively.



## Kinemage 1 -

## Kinemage 2 -


## Kinemage 3 -

Structurally the active site residues of factor XIII are very similar
to those of the cysteine proteases, represented here by actinidin.  We
use thrombin as a representative of the trypsin-related serine
proteases, to contrast with factor XIII and actinidin.  Sidechains in
factor XIII are included within about 8 A of the nucleophilic Cysteine
314.

The triad of residues compared consists of Cys 314, which is hydrogen
bonded to ND1 of His 373; the NE2 of His 373 is Hbonded in turn to OD1
of Asp 396. The Hbond arrangement is the same in actinidin.  In
contrast, alpha/beta hydrolases, subtilisin proteases, and
trypsin-related serine proteases have NE2 of histidine as the atom
closest to the Ser catalytic nucleophile.  These basic arrangements
are shown in the superimposed figures of this kinemage.

We hypothesize that NE1 of Trp 279 and the backbone N of Cys 314 are
the hydrogen bond donors which stabilize the intermediate oxyanions in
the catalytic cycle of factor XIII.  This hypothesis is based, in
part, on the similarity of the position of Gln 19 NE1 relative to the
active site Cys 25 in actinidin.  The relations among these residues
are also illustrated in this kinemage (turn on "other sc").  In Figure
4 of the text, the proposed catalytic mechanism, we have depicted the
active site Cys-His interaction as a thiolate - imidazolium ion pair.
The reason for this is that Tyr 560 OH forms a hydrogen bond with Cys
314 SG (turn on "other sc").  Such OH...S hydrogen bonds are generally
found only if the sulfur atom is charged.  The position of this Tyr
560 residue is where we suppose the glutamyl substrate is located
during the catalytic cycle.  Kinemage 3 will specifically address this
point.

## Kinemage 4 -

As in actinidin, the active site cysteine of factor XIII is at the
N-terminus of an alpha helix (View2 looks down it).  Factor XIII also
shares with actinidin four strands of beta sheet located after the
nucleophilic cysteine; the direction and order of the strands are the
same in both structures.  The L domain of the thiol proteinases
comprises more than 80 residues inserted at the position of residue
330 of factor XIII. There are also insertions in the loops of the
common beta sheet of factor XIII which contribute to secondary
structure elements in other parts of the molecule (turn on "rest" to
see how different the rest of the two molecules are).  However, the
common topology of the helix and strands bearing the catalytic triad
residues is sufficient to conclude that the proteins have diverged
from a common ancestor.  The superpositions of factor XIII and
actinidin in this kinemage are based on Calphas of the common helix
and four beta strands.

With the established relationship between the factor XIII active
site and that of the cysteine proteinases, it is plausible that the
mechanism of the transglutaminases is similar to the reverse
hydrolysis reaction of the proteinases.  In the cysteine proteinases,
the backbone amide of the active site cysteine and an invariant
glutamine residue six residues N-terminal stabilize the oxyanions by
NH..O hydrogen bonds.  Factor XIII has no residue sequentially
equivalent to this glutamine, due to the di fferent tertiary structure
preceding the active site cysteine 314.  From the superposition of the
active site side chains, Gln 19 in papain, and the carbonyl group of
leupeptin, an inhibitor of papain (Schroeder et al, 1993), we have
identified atom NE1 of tryptophan 279 and the backbone NH of cysteine
314 as the H-bond donors which could stabilize the oxyanion
intermediates as shown in this kinemage.  The distance from NE2 of Gln
19 in papain to the CO of the inhibitor that represents the transition
stat e oxyanion is 2.88 A.  Although the distance between NE1 of
tryptophan 279 and the CO of the papain inhibitor is 3.65 A, it is
conceivable that structural changes upon substrate binding to factor
XIII could shorten this distance.  (In the text the distance is given
as 3.70 A because it is the average of molecules 1 and 2; this
kinemage is constructed from molecule 2).
