<!--- -*- mode: markdown; mode: auto-fill; fill-column: 75; -*- -->

## The HIV protease and protease inhibitors ##

The HIV protease is crucial for the development of the HIV virus in that it
cleaves a viral precursor protein during maturation of the
virus. Inhibition of the protease activity therefore has the potential to
block the development of the AIDS disease.

The HIV protease belongs to the _aspartyl protease family_.

## Scene F1 ##
Scene F1 shows the native structure of HIV protease (drawn from pdb id [1hsi][]).

## Scene F2 ##
Scene F2 shows HIV protease  in complex with an (unknown) amino
acid substrate shown in stick representation with standard atom
colours. Refer to Figure 9.19 in Berg (drawn from pdb id [2hpf][])).

## Scene F3 ##
Shows a complex with the commercial protease inhibitor, indinavir (crixivan)
Refer to Figure 9.19 in Stryer for diagrams of the chemical structure of
the substrate compound (drawn from pdb id [1hsg][]).

## Questions

-  Identify the two subunits of the enzyme and describe the tertiary and
   quaternary structure of the HIV protease.

<blockquote>
   ANSWER: The enzyme consists of two identical subunits (homodimer) giving it
   an overall two-fold symmetry. Each monomer has a single domain structure
   consisting of two separate beta sheets forming a kind of beta-barrel.
</blockquote>

-  The two aspartates in the active site are: (A) Both uncharged. (B) Both
   charged. (C) One is charged and the other is uncharged.  Which is
   correct?

<blockquote>
   ANSWER:  The charged Asp activates the attacking water molecule. The
   uncharged Asp binds the substrate. (See Stryer Fig. 9.18B p. 237)
</blockquote>

-  Binding of substrate is accompanied by a large conformational change in
   the protein. Identify the parts of the enzyme which are moving. What is
   this type of movement called?

<blockquote>
   ANSWER: The loops at "the top" of the molecule move downwards to
   capture the substrate. Such accommodated binding of substrate molecules
   are called "induced fit" and usually work by making sure that the
   active site is only formed upon binding of the correct substrate.
</blockquote>

-  The catalytic reaction is carried out by two aspartates in the enzyme
   (see Figure 9.18B in Stryer). Why are two aspartates needed for the
   reaction?  Where do the the aspartates come from (hint: check the
   sequence no. by clicking them). Identify the aspartate residue
   responsible for each of the parts of the cleavage reaction (hint:
   compare figure 9.18B in Stryer).

<blockquote>
   ANSWER: One aspartate holds the carbonyl oxygen atom of the amino acid
   whose peptide bond is being cleaved while the other activates a nearby
   water molecule so it can attack the carbonyl carbon to initiate the
   cleavage. The aspartates come from both subunits of the enzyme. They
   have the same residue number.
</blockquote>

-  Look at the structures of the HIV protease in complex with a natural
   substrate (amino acids) and compare to the structure bound to
   Crixivan. Analyse the geometry at the active site. Are there
   similarities and/or differences between the two?

<blockquote>
   ANSWER: In the Crixivan structure, the oxygen atom is right between the
   aspartates, so there is no room for the activated water molecule.
</blockquote>

-  How does Crixivan bind to the enzym and what is this type of inhibition
   called? How do you propose Crixivan works to inhibit the enzyme? (hint:
   look at the hydridisation of the carbon atom near the aspartates).

<blockquote>
   Crixivan mimicks an amino acid substrate and hence binds at the same
   place. This type of inhibition is called "competitive inhibition".  The
   carbon holding the oxygen at the active site in Crixivan is sp3
   hybridised (can be seen from the non-planar arrangement of atoms), so
   this is a C-OH not a C=O. This means that there are no electron pair at
   the bond which can shift to the oxygen as required by the reaction
   mechanism and the enzyme gets stuck at a transition state conformation.
</blockquote>

[1hsi]: http://www.rcsb.org/pdb/explore/explore.do?structureId=1hsi
[2hpf]: http://www.rcsb.org/pdb/explore/explore.do?structureId=2hpf
[1hsg]: http://www.rcsb.org/pdb/explore/explore.do?structureId=1hsg
