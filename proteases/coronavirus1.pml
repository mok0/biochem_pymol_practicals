# Show the structure of the model of Coronavirus Main
# Proteinase (3CLpro) from:
# Anand, Ziebuhr, Wadhwani, Mesters & Hilgenfeld,
# www.sciencexpress.org / 13 May 2003 / Page 4/ 10.1126/science.1085658

@../include/set_default_parameters.pml

# This is a constructed MODEL so it is not in
# the ordinary repositories of the PDB, however, it is
# found in their files section:
load http://www.rcsb.org/pdb/files/1p9t.pdb, Mpro
hide everything

color wheat, Mpro
show cartoon, Mpro

# This structure is so crappy that the automatic
# assignment of secondary structure produces almost
# no secondary structure elements. We need to define
# them manually. No attempt was made to reproduce the
# figure in Anand et al. other than the orientation of
# the molecule.

# H: helix, S: sheet, L:loop
alter 12-17/, ss='H'
alter 19-22/, ss='S'
alter 25-32/, ss='S'
alter 55-60/, ss='H'
alter 66-70/, ss='S'
alter 74-76/, ss='S'
alter 79-83/, ss='S'
alter 86-90/, ss='S'
alter 100-107/, ss='S'
alter 110-117/, ss='S'
alter 124-131/, ss='S'
alter 146-153/, ss='S'
alter 155-167/, ss='S'
alter 170-175/, ss='S'

alter 202-213/, ss='H'
alter 228-237/, ss='H'
alter 248-255/, ss='H'
alter 261-271/, ss='H'
alter 293-300/, ss='H'
rebuild

spectrum count, rainbow_cycle, chain A, byres=1

set_view (\
     0.994610012,    0.045011174,    0.093193449,\
    -0.079438180,    0.909201086,    0.408684731,\
    -0.066329479,   -0.413884491,    0.907888353,\
     0.000000000,   -0.000000000, -225.142547607,\
   -20.788269043,   -2.058296204,   -5.784313202,\
   177.504104614,  272.781005859,   20.0 )

scene F1, store

set cartoon_side_chain_helper, on
create active_site, 41+145/
show sticks, active_site
util.cbaw('active_site')

set_view (\
     0.385187864,   -0.542620003,   -0.746424258,\
    -0.217057526,    0.732880294,   -0.644786716,\
     0.896928668,    0.410386413,    0.164509758,\
     0.000000000,    0.000000000,  -30.112735748,\
   -27.619314194,   13.252626419,   -6.042687416,\
    23.741109848,   36.484359741,   20.0 )

scene F2, store

scene F1, recall
