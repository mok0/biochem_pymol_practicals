# Copyright (C) 2015-2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Trypsin with benzamidine inhibitor
# Version 2.1

reinitialize

# Change default values
set dash_color, white

fetch 3ptb, async=0
hide all

create Benzamidine, resn BEN
show sticks, Benzamidine
util.cbay Benzamidine

create Trypsin, resi 16-245
color raspberry, Trypsin

# Define secondary structure, default is not very good

alter /Trypsin, ss='L'
cmd.dss("Trypsin")
#alter 18-20/, ss='S'
alter 37/, ss='S'
alter 40/, ss='S'
alter 56-58/, ss='L'
alter 62-64/, ss='S'
alter 69/, ss='S'
alter 80/, ss='S'
alter 93-96/, ss='S'
alter 111-115/, ss='S'
alter 142-146/, ss='S'
alter 148-153/, ss='S'

show cartoon, /Trypsin///16-189+195-214+219-245

# The "pocket", residues 189-195 and 214-219
create Pocket, resi 189-195+214-219
show sticks, Pocket
util.cbaw Pocket

set_view (\
    -0.265665084,   -0.316896826,    0.910493970,\
     0.133675411,    0.923196614,    0.360323787,\
    -0.954752266,    0.217436299,   -0.202898487,\
    -0.000002218,   -0.000083879, -183.452209473,\
    -3.439348459,   16.389055252,   27.423727036,\
   154.874359131,  214.037872314,  20.0 )

scene F1, store

set_view (\
    -0.772584379,    0.455063611,   -0.442754924,\
    -0.448445022,    0.102546267,    0.887905777,\
     0.449454576,    0.884534717,    0.124845244,\
    -0.000008148,    0.000010544,  -67.374404907,\
     0.220952660,   12.867823601,   17.609823227,\
    57.408058167,   87.040748596,  20.0 )

scene F2, store

create Triad, 57+102+195/ and (sidechain or name CA)
util.cbay('Triad')
set cartoon_side_chain_helper, 1
show sticks, Triad

dist Triad_Hbonds, /Triad///102/OD2, /Triad///57/ND1
dist Triad_Hbonds, /Triad///57/NE2, /Triad///195/OG
hide labels, Triad_Hbonds

#distance Hbonds, /Triad/A/A/HIS`57/ND1, /Triad/A/A/ASP`102/OD2
#distance Hbonds, /Triad/A/A/HIS`57/NE2, /Triad/A/A/SER`195/OG
#hide labels, Hbonds

set_view (\
    -0.930608094,    0.186798468,   -0.314761758,\
    -0.185285002,    0.501212239,    0.845252454,\
     0.315647960,    0.844920695,   -0.431819290,\
     0.000000000,    0.000000000,  -41.415485382,\
     4.107332706,   14.774933815,   23.301864624,\
    32.652282715,   50.178688049,   20.0 )
scene F3, store

delete 3ptb
delete (sele)
scene F1, recall
