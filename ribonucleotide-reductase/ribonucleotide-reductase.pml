# Copyright (C) 2015-2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Ribonucleotide reductase R1-R2 model from Ulla Uhlin
# Coordinates of the R1R2 holoenzyme. The file also contains effectors,
# substrate and the C-terminal R2 peptide bound in the hydrophobic
# pocket of R1.
# Version 2.1

python
def load_from_local_or_repo(fname, objnam):
    import posixpath
    bb = "https://bitbucket.org/mok0/biochem_pymol_practicals/raw/master/"
    localpath = os.path.join("pdb", fname)
    repopath =  posixpath.join(bb, "ribonucleotide-reductase", "pdb", fname)
    if os.path.exists(fname):
        cmd.load(fname, objnam)
    elif os.path.exists(localpath):
        cmd.load(localpath, objnam)
    else:
        cmd.load(repopath, objnam)
python end

reinitialize

# Change default values
set dash_color, white
set orthoscopic, on
#### Settings end ####

load_from_local_or_repo("r1r2effpep.pdb", "rnr")

hide everything

create DomA, chain A
show cartoon, DomA
color forest, DomA

create DomB, chain B
show cartoon, DomB
color marine, DomB

create DomC, chain C
show cartoon, DomC
color yellow, DomC

create DomD, chain D
show cartoon, DomD
color orange, DomD

create LigA, chain S
show sticks, LigA
util.cbaw('LigA')

create LigB, chain T
show sticks, LigB
util.cbaw('LigB')

create EffectorL, chain L
show cartoon, EffectorL
color red, EffectorL
disable EffectorL

create EffectorM, chain M
show cartoon, EffectorM
color hotpink, EffectorM
disable EffectorM

create CFe, chain C and resid 3341-3343
show spheres, CFe
util.cbaw('CFe')

create DFe, chain D and resid 3341-3343
show spheres, DFe
util.cbaw('DFe')

p1 = [60.687, 111.187, 117.337]
p2 = [-42.835, 144.762, 117.805]
radius = 0.3
r, g, b = (0.7,0.7,0.8)
cmd.load_cgo( [ 9.0, p1[0], p1[1], p1[2], p2[0], p2[1], p2[2], radius, r, g, b, r, g, b], 'axis')
delete rnr

set_view (\
    -0.016263174,    0.950558066,    0.310120702,\
    -0.186297238,   -0.307611197,    0.933096230,\
     0.982358754,   -0.042599533,    0.182089075,\
     0.000000000,   10.000000000, -347.586181641,\
    22.968719482,  123.336723328,  116.816299438,\
   274.039703369,  421.132659912,   20 )

scene F1, store

set_view (\
     0.233677149,    0.476739109,   -0.847411811,\
    -0.587726533,   -0.625043571,   -0.513705969,\
    -0.774572968,    0.618089736,    0.134135053,\
     0.000000000,    5.000000000,  -73.296302795,\
    38.732772827,  110.743324280,  132.206008911,\
    59.681735992,   86.910873413,   20 )


#disable DomB
#disable DomC
#disable DomD
color pink, /DomA//A/240-256
hide cartoon, /DomA//A/241-255
show sticks, /DomA//A/240-256 and backbone
select Loop2, /DomA//A/240-256 and backbone
util.cnc('Loop2')
deselect
scene F3, store

create Cys2, (/DomA//A/CYS`462 or /DomA//A/CYS`225 or /DomA//A/CYS`439 or /DomA//A/GLU`441) and sidechain or name CA
show sticks, Cys2
util.cbaw('Cys2')
dist Hcys2,  /LigA//S/2/O3', /DomA//A/441/OE2
hide labels, Hcys2

set_view (\
     0.237812936,   -0.604499459,   -0.760273039,\
    -0.518699527,   -0.740814567,    0.426774055,\
    -0.821207166,    0.292860717,   -0.489732116,\
     0.001823723,   -0.000427898,  -54.693775177,\
    32.687557220,  119.672294617,  143.045516968,\
    41.618057251,   67.783050537,   20 )

scene F2, store

set_view (\
     0.751637638,    0.291517437,    0.591649175,\
    -0.616721749,   -0.007393979,    0.787141383,\
     0.233842105,   -0.956530333,    0.174228355,\
     0.001870906,    0.000115122,  -92.163993835,\
     5.446968079,  139.246643066,  106.905364990,\
    81.284706116,  103.058425903,   20.0 )

#disable *
enable DomC
enable CFe
#select CFe around 5.5
select /DomC//C/2122+2118+2084+2115+2241+2204+2238 and (sidechain or name CA)
show sticks, (sele)
util.cbay('(sele)')
deselect
scene F4, store

scene F1, recall
