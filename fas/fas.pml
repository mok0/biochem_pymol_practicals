# Copyright (C) 2015-2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Yeast Fatty acid synthase, entry 2uv8
# Version 2.0

reinitialize

# Default dash-color
set dash_color, white

python
# Compatibitily with Python2
from __future__ import print_function

def draw_axis(objnam, sel1, sel2, thecol=(1.,1.,1.), radius=1.0):
    # Average coordinates from two selections and draw
    # a line between them.

    C1_25 = cmd.get_model(sel1).get_coord_list()
    C2_25 = cmd.get_model(sel2).get_coord_list()

    nfold = len(C1_25)

    p1 = [0.,0.,0.]
    p2 = [0.,0.,0.]
    for i in range(nfold):
        for j in range(3):
            p1[j] += C1_25[i][j]
            p2[j] += C2_25[i][j]
        #.
    #.

# Compute average points and length
    d = 0.0
    for j in range(3):
        p1[j] = p1[j]/nfold
        p2[j] = p2[j]/nfold
        d = d + (p2[j]-p1[j])**2
    #.
    d = math.sqrt(d)
    print("Length of axis:", d)

# Extend axis at each end by a fraction of the exis length
    ext = d/10

    for j in range(3):
        u = (p2[j] - p1[j])/d
        p1[j] -= ext * u
        p2[j] += ext * u
    #.

# Draw the symmetry axis as a CGO object
    r, g, b = thecol
    cmd.load_cgo( [ 9.0, p1[0], p1[1], p1[2],
                    p2[0], p2[1], p2[2],
                    radius, r, g, b, r, g, b ], objnam)
#.

# Load local copy if we have one
fnam = "2uv8.pdb1.gz"
path = os.path.join("pdb", fnam)
if os.path.exists(path):
    cmd.load(path, '2uv8')
else:
    url = "https://bitbucket.org/mok0/biochem_pymol_practicals/raw/master"
    url = url + "/fas/pdb/" + fnam
    cmd.load(url, '2uv8')
#.
python end

split_states 2uv8
delete 2uv8
hide everything

create wheel1, /2uv8_0001//A+B+C
color lime, wheel1
show ribbon, wheel1

create wheel2, /2uv8_0002//A+B+C
color cyan, wheel2
show ribbon, wheel2

create chamber1, /2uv8_0001//G+H+I
color marine, chamber1
show ribbon, chamber1

create chamber2, /2uv8_0002//G+H+I
color slate, chamber2
show ribbon, chamber2

draw_axis("ax_triad", "/chamber1///25/CA", "/chamber2///25/CA", (0.878, 0.320, 0.024))
sel1 = "/wheel*//B/646/CA"
sel2 = "/wheel*//C/1476/CA"
draw_axis("ax_dyad_1", sel1, sel2, (1.0, 0.820, 0.137), 0.5)
sel1 = "/wheel1//A/646/CA or  /wheel2//C/646/CA"
sel2 = "/wheel2//A/1476/CA or /wheel1//B/1476/CA"
draw_axis("ax_dyad_2", sel1, sel2, (1.0, 0.7, 0.2), 0.5)
sel1 = "/wheel2//A/646/CA or /wheel1//C/646/CA"
sel2 = "/wheel1//A/1476/CA or /wheel2//B/1476/CA"
draw_axis("ax_dyad_3", sel1, sel2, (1.0, 0.631, 0.0), 0.5)

group symm_axes, ax_triad  ax_dyad_1 ax_dyad_2 ax_dyad_3

set_view (\
    -0.839807749,    0.542440176,    0.021868223,\
    -0.365209818,   -0.534702837,   -0.762041032,\
    -0.401667833,   -0.647961080,    0.647154212,\
     0.000031352,    0.000080764, -905.075683594,\
    29.663820267,    8.398494720,    9.530792236,\
   797.962036133, 1012.210021973,  20.0 )
scene F1, store

disable chamber*
disable wheel*
hide ribbon, /wheel1 or /chamber1

# Alpha subunit
create MPT, /wheel1//A+B+C/1-94 or /chamber1//G+H+I/1661-2048
color red, MPT
show cartoon, MPT

create ACP, /wheel1//A+B+C/140-304
color purple, ACP
show cartoon, ACP

create KR, /wheel1//A+B+C/670-930
color marine, KR
show cartoon, KR

create KS, /wheel1//A+B+C/1015-1664
color cyan, KS
show cartoon, KS

# Beta chain enzymes

create AT, /chamber1//G+H+I/154-548
color green, AT
show cartoon, AT

create ER, /chamber1//G+H+I/567-1088
color yellow, ER
show cartoon, ER

create DH, /chamber1//G+H+I/1257-1658
color orange, DH
show cartoon, DH

# Not in any enzyme:
#
# Alpha subunit
# - 95-139 missing from structure
# - 305-669
# - 931-1014
# - 1665-1747
#
# Beta subunit:
#  - 1-4 missing from structure
#  - 5-153
#  - 549-566
#  - 1089-1256
#  - 1659-1660
#  - 2049-2050

create Non_enzyme, chamber1 and (resi 5-153 or resi \
       or resi 549-566 \
       or resi 1089-1256 \
       or resi 1659-1660 \
       or resi 2049-2050) \
       or wheel1 and (resi  305-669 \
       or resi 931-1014 \
       or resi 1665-1747)

color white, Non_enzyme
show ribbon, Non_enzyme

scene F2, store

# See structure from above, save as F3

set_view (\
    -0.820365369,    0.018045178,    0.571552217,\
    -0.383648217,    0.723797619,   -0.573518217,\
    -0.424036682,   -0.689771891,   -0.586861968,\
    -0.000000000,    0.000000000, -628.496948242,\
    53.954105377,   -7.643623352,  -31.373966217,\
   495.511779785,  761.482116699,   20.0 )
scene F3, store


# Binding site for flavin mononucleotide (residue FMN)

create fmn, /2uv8_0001///FMN
util.cbaw('fmn')
show sticks, fmn
create fmn_sc, /2uv8_0001//G+H+I/596-598 or /2uv8_0001//G+H+I/650+706+733+769+770+802-803+1054
util.cbay('fmn_sc')
show sticks, fmn_sc

dist Hbonds, /fmn_sc//I/596/O, /fmn//I/3051/O2'
dist Hbonds, /fmn_sc//I/598/OG1, /fmn//I/3051/N5
dist Hbonds, /fmn_sc//I/650/ND2, /fmn//I/3051/N3
dist Hbonds, /fmn_sc//I/650/OD1, /fmn//I/3051/O2
dist Hbonds, /fmn_sc//I/706/NZ, /fmn//I/3051/O2'
dist Hbonds, /fmn_sc//I/770/N, /fmn//I/3051/O1P
dist Hbonds, /fmn_sc//I/803/N, /fmn//I/3051/O3P
dist Hbonds, /fmn_sc//I/803/OG, /fmn//I/3051/O3P
# A bit long, 3.4A, but draw it anyway
dist Hbonds, /fmn_sc//I/733/OG1, /fmn//I/3051/O3'
#'
hide labels, Hbonds
group fmn_site, fmn fmn_sc Hbonds

set_view (\
     0.839219630,   -0.407766044,   -0.359774381,\
     0.543525279,    0.608336210,    0.578361392,\
    -0.016974840,   -0.680920005,    0.732163608,\
     0.000018882,    0.000044398,  -97.557716370,\
    22.576293945,  -79.859153748,  -38.054599762,\
    89.420890808,  105.694725037,  20.0 )
scene F4, store

# Binding site of PPT
create ppt, /2uv8_0001///GVL and not (name C or name O or name N)
show sticks, ppt

create triad, /2uv8_0002//A+B+C/1305+1542+1578+1583 and (sidechain or name CA)
#create triad1, /2uv8_0001//A+B+C/1305+1542+1578+1583 and (sidechain or name CA)
util.cbac("triad")
#util.cbac("triad1")
show sticks, triad

dist Hbonds2, /triad//A/1578/NZ, /triad//A/1542/ND1
hide labels, Hbonds2

group ppt_site, ppt triad Hbonds2
util.cbap("ppt_site")

enable wheel2
# show ribbon, wheel2
#show cartoon, /wheel2//A

disable fmn_site

set_view (\
     0.196054429,    0.866926134,    0.458258212,\
    -0.231333986,    0.495028228,   -0.837516129,\
    -0.952914059,    0.058188342,    0.297602206,\
    -9.000000000,    0.000000000,  -68.972007751,\
    62.235958099,    7.147564888,   57.554378510,\
    62.878421783,   75.065582275,  20.0 )
scene F5, store

# We don't need this object anymore
delete 2uv8_000*

scene F1, recall
