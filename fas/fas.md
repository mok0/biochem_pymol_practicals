# Fatty Acid Synthase

In this exercise we will take a look at the fatty acid synthase (FAS)
structure from _Saccharomyces cerevisiae_ (brewer's yeast), which can
be found in the Protein Data Bank with PDB ident [2UV8][2uv8]. The
structure was determined in 2007 by [Leibundgut _et
al._][leibundgut2007] to a resolution of 3.1 Å.

The structure of FAS from the fungus _Thermomyces lanuginosus_ was
described by [Jenni _et al._][jenni2007] in 2007.

The acyl carrier protein (ACP) domain shuttles reaction intermediates covalently
attached to its prosthetic phosphopantetheine group between the different enzymatic
centers of the reaction cycle (view F4)

# Questions

1.  Where in the cell does fatty acid synthesis take place?

1.  Where does fatty acid degradation take place?

1.  In view F1, describe the organization of yeast FAS.

1.  In view F2, several objects have been defined (MPT, ACP, etc). What
   do the object names refer to? Write down what reactions take place in
   each of these objects.

1. In _E. coli_, ACP is a separate enzyme, whereas in yeast FAS, ACP
   is an intergral part of the enzyme. What is the role of ACP and
   what is the advantage of this arrangement?

1. In view F3, look at the flavin mononucleotide. How does the
   part of the aromatic ring structure that contains carbons C6-C9 interact
   with the the protein?

1. In view F3, look at the way Asn-650 interacts with the flavin moiety. Is
   there an error in the structure, and if so, what could it be?

1. In view F4, part of the prosthetic phosphopantetheine group is seen
   (the outer part of the PPT group is missing), as well as the catalytic
   cleft of KS. What is the role of the PPT? What part of the FAS complex does
   PPT interact with?

[2uv8]: http://www.rcsb.org/pdb/explore/explore.do?structureId=2uv8
[leibundgut2007]: http://www.sciencemag.org/content/316/5822/288.full
[jenni2007]: http://www.sciencemag.org/content/316/5822/254.long
