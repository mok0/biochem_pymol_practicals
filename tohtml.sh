#! /bin/sh

# Create a web page from the markdown source

case $# in
    0) echo "Usage: $0 files" 1>&2; exit 1
esac

# Loop over arguments
for i; do
    file=$i

    if head -2 $file|grep -s 'mode: org' > /dev/null ; then
	orgmode=1
	mode="org"
    else
	orgmode=0
	mode="markdown"
    fi

    echo "$file ($mode)"
    if [ -r $file ]; then
	base=`basename $file .md`
        cat <<EOF >> tmp.html
<html><head>
<link href="http://kevinburke.bitbucket.org/markdowncss/markdown.css" rel="stylesheet"></link></head><body>
EOF
# Another style:
# https://raw.githubusercontent.com/markdowncss/modest/master/css/modest.css
        if [ $mode = "org" ]; then
            sed '1d' $file | sed 's/\*/#/g' | markdown >> tmp.html
	else
            cat $file | markdown >> tmp.html
	fi
	echo "</body></html>" >> tmp.html
	sed 's/ -- / \&mdash; /g' < tmp.html | sed 's/^-- /\&mdash; /g' | sed 's/ --$/ \&mdash;/g'|sed 's/<p>--/<p>\&mdash;/g'|sed 's/--<\/p>/\&mdash;<\/p>/'|sed 's/\.\.\./\&\#133;/g'> ${base}.html
    fi
done
rm -f tmp.html
