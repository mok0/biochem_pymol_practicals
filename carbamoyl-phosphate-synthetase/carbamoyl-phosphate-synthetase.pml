# Copyright (C) 2015-2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Carbamoyl phosphate synthetase
# Version 2.0

# The original goal in the crystallization of the site-directed mutant
# protein from entry 1A9X (namely, H353N) was to explore the mode of
# binding of glutamine in the CPS small subunit active site pocket.
# From previous biochemical studies it is known that the replacement
# of this histidine with an asparagine residue results in a form of
# CPS unable to utilize glutamine as a substrate.
# See Thoden et al. (1998) Biochemistry 37, 8825-8831.

reinitialize

# Change default values
set dash_color, white
set orthoscopic, on
#### Settings end ####

fetch 1a9x, async=0
hide everything

create CPS, chain A+B

# Domain are definitions from:
# Guillou et al. (1989), PNAS 86, 8304-8308.

### Glutaminase (chain B) ###
color white, chain B

# Subunit interations 1-114

#  Domain catalyzing glutamine amide-N transfer 204-382
#  (Resid numbers offset by 1500 relative to Guillou et al.)
color yellow, chain B and resid 1704-1882

### Synthetase (chain A) ###
color white, chain A

# Subunit interactions 1-149

# ATP-CO2 domain
color cyan, chain A and resid 150-352
OB
# Subunit interaction 579-697

# ATP-CO2
color hotpink, chain A and resid 698-885

# Allosteric Binding Domain
#color lightpink, chain A and resid 886-1073
color white, chain A and resid 886-1073

####

show cartoon, CPS


create Gln, chain A and resid 1091-1092
# Not sure what 1091 is? Random binding?
show sticks, Gln
util.cbaw('Gln')

##########

create AD1, /CPS//A/129+169+176+208+210+215+241-243+285+299 or chain A and resid 1900
show sticks, AD1
util.cbaw('AD1')

dist Had1, /AD1//A/1900/N6, /AD1//A/208/O
dist Had1, /AD1//A/1900/N1, /AD1//A/210/N
dist Had1, /AD1//A/1900/O2', /AD1//A/215/OE1
dist Had1, /AD1//A/1900/O3', /AD1//A/215/OE2
dist Had1, /AD1//A/1900/O1A, /AD1//A/285/OE1
dist Had1, /AD1//A/1900/O1A, /AD1//A/285/NE2
dist Had1, /AD1//A/1900/O3B, /AD1//A/299/OE1
dist Had1, /AD1//A/1900/O3B, /AD1//A/299/OE2
dist Had1, /AD1//A/1900/O3B, /AD1//A/129/NH2
dist Had1, /AD1//A/1900/O2B, /AD1//A/176/N
dist Had1, /AD1//A/1900/O1B, /AD1//A/243/NE2
dist Had1, /AD1//A/1900/O2A, /AD1//A/169/OE1

dist Had1, /AD1//A/129/NH1, /AD1//A/299/OE1
hide labels, Had1

# Create carbamoyl-phosphate site (See Stryer fig. 25.4)
# (called AD2 in PDB file)
create CP-site, /CPS//A/715+754+756+761+786-788+841 or chain A and resid 1910
show sticks, CP-site
util.cbaw('CP-site')

dist Hcp-site, /CP-site//A/1910/N1, /CP-site//A/756/N
dist Hcp-site, /CP-site//A/1910/N6, /CP-site//A/754/O
dist Hcp-site, /CP-site//A/1910/N7, /CP-site//A/715/NH2
dist Hcp-site, /CP-site//A/1910/O1A, /CP-site//A/715/NH1
dist Hcp-site, /CP-site//A/1910/O2A, /CP-site//A/841/OE2
dist Hcp-site, /CP-site//A/1910/O3B, /CP-site//A/841/OE2
dist Hcp-site, /CP-site//A/1910/O1B, /CP-site//A/788/NE2
dist Hcp-site, /CP-site//A/1910/O2', /CP-site//A/761/OE1
dist Hcp-site, /CP-site//A/1910/O3', /CP-site//A/761/OE2
hide labels, Hcp-site

create OR1, /CPS//A/783+791+892+1040+1042 or chain A and resid 1920
show sticks, OR1
util.cbaw('OR1')
dist Hor1, /OR1//A/1920/N, /OR1//A/1040/O
dist Hor1, /OR1//A/1920/O, /OR1//A/1042/OG1
dist Hor1, /OR1//A/1920/OXT, /OR1//A/1042/OG1
dist Hor1, /OR1//A/1920/NE, /OR1//A/783/OE2
dist Hor1, /OR1//A/1920/NE, /OR1//A/791/O
dist Hor1, /OR1//A/1920/NE, /OR1//A/892/OE1
hide labels, Hor1

create SY1, /CPS//B/1547+1741+1743+1773+1813-1814 or chain B and resid 1769
show sticks, SY1
util.cbaw('SY1')
dist Hcy1, /SY1//B/1769/N1, /SY1//B/1743/O
dist Hcy1, /SY1//B/1769/N1, /SY1//B/1741/O
dist Hcy1, /SY1//B/1769/O1, /SY1//B/1813/N
dist Hcy1, /SY1//B/1769/O2, /SY1//B/1813/N
dist Hcy1, /SY1//B/1769/O2, /SY1//B/1773/NE2
dist Hcy1, /SY1//B/1769/OE2, /SY1//B/1741/N
hide labels, Hcy1

# Site directed mutation on this structure
create H353N, chain B and resid 1853
show sticks, H353N
util.cbam('H353N')

# Tetraethylammonium ion
#create NET, chain A and resid 1950
#show sticks, NET
#util.cbaw('NET')
##########

# Make a tube representing the tunnel
txt = """ \
    32.303    35.461    44.962 \
    39.328    35.684    49.835 \
    43.124    32.615    52.313 \
    46.093    27.679    53.844 \
    43.578    22.790    57.395 \
    41.253    23.837    58.930 \
    38.501    24.473    62.228 \
    33.811    27.173    62.595 \
    31.865    29.478    65.002 \
    28.337    31.091    66.817 \
    26.099    30.047    68.560 \
    23.465    30.039    69.861 \
    21.660    31.765    72.916 \
    23.645    34.593    73.355 \
    27.506    35.783    72.974 \
    27.626    38.173    73.693 \
    28.307    40.849    75.785 \
    29.081    40.560    77.984 \
    28.897    41.460    79.063 \
    28.961    43.844    81.930 \
    28.238    44.191    83.216 \
    27.499    44.752    84.063 \
    25.557    46.030    84.106 \
    24.590    45.888    84.319 \
    23.321    46.276    84.855 \
    22.909    46.777    88.129 \
    23.405    47.925    91.610 \
    23.167    50.284    94.851 \
    23.387    51.831    97.740 \
    23.865    53.926    99.927 \
    23.573    55.690   101.982 \
    23.778    55.909   104.602 \
    24.260    54.425   108.057"""
m = [float(i) for i in txt.split()]
SPHERE = 7.0
CYLINDER =  9.0
RADIUS = 1.0
obj = []
for i in range(0,99,3): \
    obj.append(SPHERE) \
    obj.append (m[i]) \
    obj.append(m[i+1]) \
    obj.append(m[i+2]) \
    obj.append(RADIUS) \

for i in range(0,99,3): \
    obj.append(CYLINDER) \
    for j in range(6): \
    	obj.append(m[i+j]) \
    obj.append(RADIUS) \
    obj.extend([1,0.6,0]) \
    obj.extend([1,0.6,0])

cmd.load_cgo(obj,'Tunnel')
color orange, Tunnel


delete 1a9x
disable Gln
disable AD1
disable Had1
disable CP-site
disable Hcp-site
disable OR1
disable Hor1
disable SY1
disable Hcy1
disable H353N
disable Tunnel


set_view (\
    -0.862171531,   -0.239969701,   -0.446177334,\
    -0.500572860,    0.539137125,    0.677314460,\
     0.078015439,    0.807306588,   -0.584951937,\
     0.000039522,   -0.000043124, -353.319213867,\
    30.345127106,   36.036773682,   75.262969971,\
   284.453887939,  422.183868408,   20.0 )

scene F1, store

enable Tunnel
scene F2, store

# Create alignment
create ATP-dom1, chain A and resid 698-885
create ATP-dom2, chain A and resid 150-352
align ATP-dom2, ATP-dom1

disable Tunnel
disable CPS
enable CP-site
enable Hcp-site
enable CPS

set_view (\
  -0.419363648,   0.466114372,   0.779018641,\
   0.355333388,   0.873936355,  -0.331622452,\
  -0.835387290,   0.137741148,  -0.532122970,\
   0.000000000,   0.000000000,-164.643936157,\
  51.066509247,  20.889812469,  49.829170227,\
 142.358016968, 186.929840088,  20 )

scene F3, store

# SY1 view
enable SY1
enable Hcy1
enable CPS

set_view (\
    -0.794045746,    0.596899033,   -0.114880063,\
    -0.607820749,   -0.781548500,    0.140445441,\
    -0.005948993,    0.181342527,    0.983403087,\
    -0.000000000,    0.000000000,  -47.421180725,\
    26.104333878,   55.656463623,  108.244369507,\
    40.968906403,   53.873455048,   20.000000000 )
scene F4, store

# OR1 view
disable SY1
disable Hcy1
enable OR1
enable Hor1
set_view (\
     0.875299335,    0.431657314,   -0.217997774,\
     0.222519487,    0.040716540,    0.974077642,\
     0.429343849,   -0.901118279,   -0.060413003,\
     0.000000000,    0.000000000,  -49.635982513,\
    33.395835876,   15.027561188,   51.724567413,\
    39.133384705,   60.138580322,   20.000000000 )
scene F5, store

# AD1 view
disable OR1
disable Hor1
enable AD1
enable Had1

set_view (\
     0.983949780,   -0.178430945,    0.001216206,\
    -0.171386123,   -0.943157315,    0.284740686,\
    -0.049658071,   -0.280380487,   -0.958602011,\
     0.000000000,    0.000000000,  -57.034736633,\
    14.383120537,   33.908115387,   70.274040222,\
    46.803436279,   67.266014099,   20.0 )

scene F6, store

# CP-site view
disable AD1
disable Had1
enable CP-site
enable Hcp-site
set_view (\
     0.214770406,   -0.317479879,   -0.923623383,\
    -0.289322376,   -0.923925579,    0.250307620,\
    -0.932826996,    0.213466257,   -0.290285915,\
     0.000000000,    0.000000000,  -67.970275879,\
    45.436290741,   23.111816406,   45.013992310,\
    53.588283539,   82.352264404,   20.0 )

scene F7, store

scene F1, recall
