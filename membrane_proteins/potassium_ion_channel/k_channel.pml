# Copyright (C) 2015-2017 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Potassium Ion Channel
# Version 2.3

python
def load_from_local_or_repo(fname, objnam):
    import posixpath
    bb = "https://bitbucket.org/mok0/biochem_pymol_practicals/raw/master/"
    localpath = os.path.join("pdb", fname)
    repopath = posixpath.join(bb, "membrane_proteins", "potassium_ion_channel", "pdb", fname)
    if os.path.exists(fname):
        cmd.load(fname, objnam)
    elif os.path.exists(localpath):
        cmd.load(localpath, objnam)
    else:
        cmd.load(repopath, objnam)
python end

reinitialize

# Change default values
set dash_color, white
set nb_spheres_size, 0.6

load_from_local_or_repo('1k4c.pdb1.gz', '1k4c')
split_states 1k4c
delete 1k4c
hide everything

# Color each subunit i a beautiful color
color red, 1k4c_0001
color green, 1k4c_0002
color marine, 1k4c_0003
color yellow, 1k4c_0004

python
abcd = "ABCD"
for i in range(4):
    sel = '/1k4c_{:04d}//C'.format(i+1)
    obj = 'mem_{}'.format(i+1)
    cmd.create(obj, sel)
    cmd.show_as('cartoon', obj)
    cmd.group(abcd[i], members=obj, action='add', quiet=0)
#.

for i in range(4):
    obj = 'cyt_{}'.format(i+1)
    sel = '/1k4c_{:04d}//A+B'.format(i+1)
    cmd.create(obj, sel)
    cmd.show_as('cartoon', obj)
    cmd.disable(obj)
    cmd.group(abcd[i], members=obj, action='add', quiet=0)
#.

# Draw the four-fold symmetry axis. We fetch the 4 CA's of residues
# B202 and C124, average them, and draw a line between these 2 points.

B202 = cmd.get_model('B/202/CA').get_coord_list()
C124 = cmd.get_model('C/124/CA').get_coord_list()

p1 = [0.,0.,0.]
p2 = [0.,0.,0.]
for i in range(4):
    for j in range(3):
        p1[j] += B202[i][j]
        p2[j] += C124[i][j]
    #.
#.

# Compute average points
for j in range(3):
    p1[j] = p1[j]/4
    p2[j] = p2[j]/4
#.
radius = 0.25
r, g, b = (0.9, 0.9, 1.0)
cmd.load_cgo( [ 9.0, p1[0], p1[1], p1[2],
                p2[0], p2[1], p2[2],
                radius, r, g, b, r, g, b ], "symm_axis" )
python end

cmd.enable('symm_axis')

# Look down fourfold axis
set_view (\
     0.0, 1.0, 0.0,\
    -1.0, 0.0, 0.0,\
     0.0, 0.0, 1.0,\
     0.000000000,    0.000000000, -173.0,\
   155.330261230,  155.329788208,   -8.685640335,\
     6.494459152,  340.235473633,   20.0 )

scene F2, store

cmd.disable('symm_axis')

turn x, -90
zoom all, -30
move y, 40
scene F1, store

# Create object containing 7 K´+ sites from subunit 2. (There are 4
# copies of each of the 7 K+ sites in the biological units file, we
# choose the green one.)

create K, /1k4c_0002 and name K
show nb_spheres, K
color purple, K

create H2O, resid 3009-3011
show nb_spheres, H2O
color red, H2O

create TVGYG, chain C and resid 75-79
show sticks, TVGYG
util.cnc('TVGYG')
select chain C and resid 76-78
hide cartoon, (sele)

# Partially solvated
dist Hbond, /K//C/3006, ///C/3010/O

# Channel begin
dist Hbond, /K//C/3001, ///C/78/O
dist Hbond, /K//C/3001, ///C/77/O

dist Hbond, /K//C/3002, ///C/77/O
dist Hbond, /K//C/3002, ///C/76/O

dist Hbond, /K//C/3003, ///C/76/O
dist Hbond, /K//C/3003, ///C/75/O

dist Hbond, /K//C/3004, ///C/75/O
dist Hbond, /K//C/3004, ///C/75/OG1
# Channel end

# Fully solvated
dist Hbond, /K//C/3005, ///C/3009/O
dist Hbond, /K//C/3005, ///C/3011/O
hide labels, Hbond

color grey80, 1k4c_000*

set_view (\
    -0.246371388,   -0.066816747,    0.966853738,\
     0.969139695,   -0.020648686,    0.245536610,\
     0.003554337,    0.997520447,    0.069848813,\
    -0.005709695,    0.003423460,  -93.062759399,\
   151.766052246,  154.611053467,  -38.975280762,\
    81.607894897,   98.212524414,   20.0 )

scene F3, store

set_view (\
    -0.385932893,   -0.290897816,    0.875445545,\
     0.922495782,   -0.121339388,    0.366364479,\
    -0.000353217,    0.948995411,    0.315189242,\
     0.000000000,    0.000000000, -101.908821106,\
   155.328048706,  155.329910278,  -37.443683624,\
    84.546401978,  119.271240234,   20.0 )

create VdW, K or H2O
show spheres, VdW

scene F4, store

python
for i in range(4):
    cmd.enable("cyt_{}".format(i+1))
#.
python end

cmd.enable('symm_axis')

set_view (\
    -0.652120054,   -0.000000456,   -0.758115530,\
    -0.758115411,   -0.000000093,    0.652120233,\
    -0.000000368,    0.999999940,   -0.000000284,\
    -0.000000000,    0.000000000, -406.324920654,\
   155.330001831,  155.330001831,  -24.203998566,\
   310.117431641,  502.532531738,   20.000000000 )

scene F5, store

turn y, 170
scene F6, store

turn y, 170
scene F7, store

# We don't need these objects
delete 1k4c_*

scene F1, recall
