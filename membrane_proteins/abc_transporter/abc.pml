# Copyright (C) 2016-2017 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Trypsin with pancreatic trypsin inhibitor
# Version 1.4

python
def load_from_local_or_repo(fname, objnam):
    import posixpath
    bb = "https://bitbucket.org/mok0/biochem_pymol_practicals/raw/master/"
    localpath = os.path.join("pdb", fname)
    repopath =  posixpath.join(bb, "membrane_proteins", "abc_transporter", "pdb", fname)
    if os.path.exists(fname):
        cmd.load(fname, objnam)
    elif os.path.exists(localpath):
        cmd.load(localpath, objnam)
    else:
        cmd.load(repopath, objnam)
python end

reinitialize

# Change default values
set dash_color, white

# Load entries 3b60 and 3g5u from local file, otherwise download.
load_from_local_or_repo("3g5u.pdb1.gz", "mrp1a")
load_from_local_or_repo("3b60.pdb1.gz", "msbA")

hide everything

alter /msbA///219-222, ss='L'
alter /msbA///118-120, ss='L'

align mrp1a, msbA
show cartoon, mrp1a
show cartoon, msbA

# Membrane-spanning domain
color teal, /mrp1a//A/33-370
color marine, /mrp1a//A/684-1011

# ATP-binding domains
color red, /mrp1a//A/378-626
color orange, /mrp1a//A/1018-1271

# Linker
color grey, /mrp1a//A/371-377
color grey, /mrp1a//A/1012-1017


# Membrane-spanning domain
color teal, /msbA//A/10-324
color marine, /msbA//B/10-324

# ATP-binding domain
color red, /msbA//A/333-581
color orange, /msbA//B/333-581

# Linker
color grey, /msbA//A/325-332
color grey, /msbA//B/325-332


# Scene F1 #

set_view (\
     0.410061955,   -0.256403804,   -0.875276208,\
     0.680901766,    0.724553943,    0.106747657,\
     0.606813490,   -0.639749706,    0.471697986,\
     0.000078857,   -0.000084803, -416.256164551,\
   111.674430847,   -6.698148727,   71.912078857,\
   377.651000977,  454.849975586,  20 )

turn z, 180
turn y, -50

disable msbA
scene F1, store

# Scene F2 #
enable msbA
disable mrp1a
scene F2, store

# Show ATP from msbA
select resn ANP
show stick, (sele)
util.cbay('(sele)')

# Walker A motif from 3B60: GRSGSGKS
create WalkerA, /msbA//A/376-383
show sticks, WalkerA
util.cbaw('WalkerA')
hide cartoon, /msbA//A/377-382
hide cartoon, WalkerA

select /msbA//B/482
show sticks, (sele)
util.cnc('(sele)')

distance HbWA, /msbA//A/ANP`5001/O3', /msbA//A/GLY`379/O
# prime O3'
distance HbWA, /msbA//A/ANP`5001/O1B, /msbA//A/LYS`382/NZ
distance HbWA, /msbA//A/ANP`5001/O2G, /msbA//A/SER`378/OG
distance HbWA, /msbA//A/ANP`5001/O2A, /msbA//A/SER`380/O
distance HbWA, /msbA//A/ANP`5001/O2B, /msbA//A/SER`383/OG

# B subunit appears to sense presence of gamma phosphate
distance HbWA, /msbA//A/ANP`5001/O2G, /msbA//B/SER`482/OG

hide labels, HbWA

set_view (\
               0.181207046,    0.099825777,   -0.978365541,\
              -0.803897083,    0.588090062,   -0.088887580,\
               0.566490769,    0.802611113,    0.186816975,\
               0.000000000,    0.000000000,  -53.418888092,\
              88.809997559,    8.781999588,   47.113998413,\
              22.153350830,   84.684425354,   20.000000000 )
move x, 5
scene F3, store

select /msbA//A/351
show sticks, (sele)
util.cbaw('(sele)')
move x, -5
scene F4, store

# Walker B motif: ILILDE
create WalkerB, /msbA//A/501-506
show sticks, WalkerB
color raspberry, WalkerB
util.cnc('WalkerB')
hide cartoon,  /msbA//A/502-505
hide cartoon, WalkerB
distance HbWB,  /msbA//A/ASP`505/OD1, /msbA//A/SER`383/OG
hide labels, HbWB

set_view (\
     0.353187621,    0.863854527,    0.359178811,\
    -0.427129328,    0.490464777,   -0.759603560,\
    -0.832353234,    0.114869043,    0.542206407,\
    -0.000080496,    0.000104441,  -67.491950989,\
    98.504013062,   11.915626526,   40.294807434,\
    57.333358765,   77.646865845,   20.000000000 )

scene F5, store

scene F1, recall
select none
