
# Computer practicals in Advanced Biochemistry

7th revised edition. August 2015

The idea behind these exercises is to give you an idea of the importance of three-dimensional protein structures in the understanding of molecular biology and metabolism. The computer practicals consist of three parts; first, we will study into the details of the catalytic mechanisms of the serine proteases as described in Chapter 9 of Berg, Tymoczko and Stryer. Then, in the second part, we will look at allosteric regulation of multi-subunit enzymes exemplified by aspartate transcarbamoylase (ATCase), that we encountered in Chapter 10 and glycogen phosphorylase that you saw in Chapter 21. Finally, in the third part, we will look at the structure of RNase A that is used for the lab practicals, and reach some conclusions from the structures that will be useful for your lab report.

The computer exercises have a long and proud history at the department. They were originally introduced by Jens Nyborg, who wanted a strong structural biology aspect in the course using the program, [Rasmol][rasmol]. In 2003, Morten Kjeldgaard and Ditlev Brodersen reworked the exercises for use with the kinemage system, [KiNG][king]. Most recently, in 2008, the exercises have been updated and transferred to [PyMOL][pymol] by Kasper Andersen and Ditlev Brodersen.

The reasons for using PyMOL are manifold. First and foremost, this is the tool that most researchers in structural biology use today, which means that you will learn to use a state-of-the-art scientific tool. Secondly, we want to standardise the molecular graphics software used in molecular biology at the university, so that the students use the same program for molecular visualisation across all courses. Finally, the program can be downloaded for free from the internet, which enables students to work and practice their skills at home at no extra cost.

In the first part of the practical, we will be studying the catalytic mechanism of the serine proteases that we encountered in Chapter 9 of Berg, Tymoczko and Stryer. We will look at the active site residues and examine the action of specific inhibitors. We will also look at the evolutionary aspects by comparing proteases from distant organisms.

[rasmol]: http://www.rasmol.org
[pymol]: http://www.pymolwiki.org/
[king]: http://kinemage.biochem.duke.edu/software/king.php
