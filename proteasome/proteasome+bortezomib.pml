# Copyright (C) 2015-2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Complex of yeast 20S proteasome with bortezomib at 2.8 A resolution
# Version 2.0

reinitialize

# Change default values
set dash_color, white
#set orthoscopic, on
# End settings

cmd.set_color("winter_1", [0.000, 0.298, 0.851])
cmd.set_color("winter_2", [0.000, 0.400, 0.800])
cmd.set_color("winter_3", [0.000, 0.502, 0.749])
cmd.set_color("winter_4", [0.000, 0.600, 0.700])
cmd.set_color("winter_5", [0.000, 0.702, 0.649])
cmd.set_color("winter_6", [0.000, 0.800, 0.600])
cmd.set_color("winter_7", [0.000, 0.902, 0.549])

cmd.set_color("summer_1", [0.200, 0.600, 0.400])
cmd.set_color("summer_2", [0.298, 0.649, 0.400])
cmd.set_color("summer_3", [0.400, 0.700, 0.400])
cmd.set_color("summer_4", [0.502, 0.751, 0.400])
cmd.set_color("summer_5", [0.600, 0.800, 0.400])
cmd.set_color("summer_6", [0.702, 0.851, 0.400])
cmd.set_color("summer_7", [0.800, 0.900, 0.400])

cmd.set_color("spring_1", [1.000, 0.200, 0.800])
cmd.set_color("spring_2", [1.000, 0.298, 0.702])
cmd.set_color("spring_3", [1.000, 0.400, 0.600])
cmd.set_color("spring_4", [1.000, 0.502, 0.498])
cmd.set_color("spring_5", [1.000, 0.600, 0.400])
cmd.set_color("spring_6", [1.000, 0.702, 0.298])
cmd.set_color("spring_7", [1.000, 0.800, 0.200])

cmd.set_color("autumn_1", [1.000, 0.200, 0.000])
cmd.set_color("autumn_2", [1.000, 0.298, 0.000])
cmd.set_color("autumn_3", [1.000, 0.400, 0.000])
cmd.set_color("autumn_4", [1.000, 0.502, 0.000])
cmd.set_color("autumn_5", [1.000, 0.600, 0.000])
cmd.set_color("autumn_6", [1.000, 0.702, 0.000])
cmd.set_color("autumn_7", [1.000, 0.800, 0.000])

fetch 2f16, async=0
hide everything

# A B C D E F G
create Alpha1, chain A+B+C+D+E+F+G
color winter_1, /Alpha1//A
color winter_2, /Alpha1//B
color winter_3, /Alpha1//C
color winter_4, /Alpha1//D
color winter_5, /Alpha1//E
color winter_6, /Alpha1//F
color winter_7, /Alpha1//G

# H I J K L M N
create Beta1, chain H+I+J+K+L+M+N
color autumn_1, /Beta1//H
color autumn_2, /Beta1//I
color autumn_3, /Beta1//J
color autumn_4, /Beta1//K
color autumn_5, /Beta1//L
color autumn_6, /Beta1//M
color autumn_7, /Beta1//N

# S R Q P O U T
create Alpha2, chain O+P+Q+R+S+T+U
color winter_1, /Alpha2//S
color winter_2, /Alpha2//R
color winter_3, /Alpha2//Q
color winter_4, /Alpha2//P
color winter_5, /Alpha2//O
color winter_6, /Alpha2//U
color winter_7, /Alpha2//T

# 1 Z Y X W V 2
create Beta2, chain V+W+X+Y+Z+1+2
color autumn_7, /Beta2//2
color autumn_6, /Beta2//V
color autumn_5, /Beta2//W
color autumn_4, /Beta2//X
color autumn_3, /Beta2//Y
color autumn_2, /Beta2//Z
color autumn_1, /Beta2//1



show cartoon, Alpha1
show cartoon, Alpha2
show cartoon, Beta1
show cartoon, Beta2

group Top, Alpha1 Beta1
group Bottom, Alpha2 Beta2

orient
turn z, -90
zoom all, 10

scene F1, store

# Create object containing bortezomib sites, found in chains H+K+N and V+Y+2

create BO2, resnam BO2
show sticks, BO2
util.cbaw('BO2')

dist Hbonds, /BO2//K/1402/O8, /Beta1//K/49/N
dist Hbonds, /BO2//K/1402/N9, /Beta1//K/21/O
dist Hbonds, /BO2//K/1402/O19, /Beta1//K/21/N
dist Hbonds, /BO2//K/1402/N20, /Beta1//K/47/O
dist Hbonds, /BO2//K/1402/O27, /Beta1//K/47/N
dist Hbonds, /BO2//K/1402/N4, /Beta1//L/114/OD2
dist Hbonds, /BO2//K/1402/N1, /Beta1//K/21/O
hide labels, Hbonds

dist Hbonds2, /Beta1//K/1/OG1, /Beta1//K/33/NZ
dist Hbonds2, /Beta1//K/19/O, /Beta1//K/33/NZ
dist Hbonds2, /Beta1//K/1/N, /Beta1//K/168/O

dist Hbonds2, /Beta1//K/2/N, /Beta1//K/17/OD2
dist Hbonds2, /Beta1//K/2/O, /Beta1//K/17/N
dist Hbonds2, /Beta1//K/4/N, /Beta1//K/15/O
dist Hbonds2, /Beta1//K/4/O, /Beta1//K/15/N

dist Hbonds2, /Beta1//K/6/N, /Beta1//K/13/O
dist Hbonds2, /Beta1//K/6/O, /Beta1//K/13/N
dist Hbonds2, /Beta1//K/8/N, /Beta1//K/11/O

dist Hbonds2, /Beta1//K/20/N, /Beta1//K/28/O
dist Hbonds2, /Beta1//K/20/O, /Beta1//K/28/N
dist Hbonds2, /Beta1//K/20/O, /Beta1//K/27/N
dist Hbonds2, /Beta1//K/22/N, /Beta1//K/25/O
dist Hbonds2, /Beta1//K/22/O, /Beta1//K/25/N

dist Hbonds2, /Beta1//K/46/N, /Beta1//K/98/O
dist Hbonds2, /Beta1//K/46/O, /Beta1//K/98/N
dist Hbonds2, /Beta1//K/48/N, /Beta1//K/96/O

hide labels, Hbonds2

# Initally, don't show
disable Hbonds2

hide cartoon, Beta1
show sticks, Beta1
util.cnc('Beta1')
util.cbaw('(Beta1 and resid 1402)')

set_view (\
     0.644695818,   -0.091832317,    0.758900166,\
     0.763727427,    0.120111361,   -0.634262383,\
    -0.032907076,    0.988502562,    0.147571623,\
     0.000000000,    0.000000000,  -66.990806580,\
    11.539072037, -138.282974243,   15.101891518,\
    60.682277679,   73.299339294,   20.0 )

scene F2, store

scene F1, recall
