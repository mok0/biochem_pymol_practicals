# Copyright (C) 2015-2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Tryptophan synthase
# Salmonella enterica
# Version 2.0

reinitialize

# Change default values
set dash_color, white

fetch 1qop, async=0
hide everything

select chain A
color yellow, (sele)
select chain B
color marine, (sele)

show cartoon, 1qop

create PLP, resnam PLP or (///B/87 and sidechain or name ca)
util.cbaw('PLP')
show sticks, PLP

# Indole propanole phosphate (IPL)
create IPL, resnam IPL
util.cbaw('IPL')
show sticks, IPL

set_view (\
     0.416629761,   -0.899941444,   -0.128537461,\
     0.764232397,    0.423300147,   -0.486586481,\
     0.492312580,    0.104493193,    0.864125431,\
     0.000000000,    0.000000000, -280.411071777,\
    66.721336365,   17.750152588,   15.836208344,\
   223.513885498,  337.308227539,  20.0 )

scene F1, store

select ///B/232-235
hide cartoon, (sele)
select ///B/231-236 or ///B/377
show sticks, (sele)
util.cbab('sele')

dist Hbonds, B/500/O1P, B/235/N
# O1P must be protonated:
dist Hbonds, B/500/O1P, B/235/OG
dist Hbonds, B/500/O2P, B/233/N
dist Hbonds, B/500/O2P, B/232/N
dist Hbonds, B/500/O3P, B/236/N
dist Hbonds, B/500/O3P, B/236/ND2
dist Hbonds, B/500/N1, B/377/OG
# Internal Hbond:
dist Hbonds, B/500/O3, B/87/NZ
hide labels, Hbonds
deselect
dele sele

set_view (\
    -0.162070215,   -0.967704356,   -0.193064526,\
    -0.669472814,   -0.035906442,    0.741964936,\
    -0.724941850,    0.249501541,   -0.642035663,\
    -0.000014991,    0.000705221,  -55.693889618,\
    82.559898376,   12.537910461,   13.830245972,\
    49.245388031,   62.140254974,   20.0 )

scene F2, store

set_view (\
    -0.264490455,   -0.726409495,    0.634324372,\
     0.962884724,   -0.235639289,    0.131640300,\
     0.053847875,    0.645598888,    0.761776149,\
     0.000133077,    0.000306621,  -42.430164337,\
    49.721405029,   26.525276184,   11.701326370,\
    23.553075790,   61.286106110,   20.0)

dist Hbonds2, A/300/N1, A/60/OD1
dist Hbonds2, A/300/O3P, A/213/N
dist Hbonds2, A/300/O3P, A/184/N
dist Hbonds2, A/300/O1P, A/235/N
dist Hbonds2, A/300/O1P, A/235/OG
dist Hbonds2, A/300/O2P, A/234/N
hide labels, Hbonds2

select ///A/184 or ///A/213 or ///A/234-235
show sticks, (sele)
util.cbay('sele')

select ///A/60 and (sidechain or name ca)
show sticks, (sele)
util.cbay('sele')
deselect

scene F3, store

scene F1, recall
