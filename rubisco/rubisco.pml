# Copyright (C) 2016-2017 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Rubisco
# Version 1.2

python
def load_from_local_or_repo(fname, objnam):
    import posixpath
    bb = "https://bitbucket.org/mok0/biochem_pymol_practicals/raw/master/"
    localpath = os.path.join("pdb", fname)
    repopath =  posixpath.join(bb, "rubisco", "pdb", fname)
    if os.path.exists(fname):
        cmd.load(fname, objnam)
    elif os.path.exists(localpath):
        cmd.load(localpath, objnam)
    else:
        cmd.load(repopath, objnam)
python end

reinitialize

# Change default values
set dash_color, white

# Load file from biological entities
load_from_local_or_repo("8ruc.pdb1.gz", "8ruc")

hide everything

split_states 8ruc
delete 8ruc


python
# Structure is split in identical top and bottom parts
topobj = {'Top': '8ruc_0001', 'Bottom': '8ruc_0002'}

# Each part contains 4 large and 4 small chains
chains = {'L': ['A', 'C', 'E', 'G'], 'S': ['I', 'J', 'K', 'L']}

for p in topobj.keys():
    cmd.group(p, quiet=0)
    for u in ('L', 'S'):
        o = p + '_' + u
        s = "/{}//{}".format(topobj[p], '+'.join(chains[u]))
        cmd.create(o, s)
        cmd.show_as("cartoon", o)
        if u == 'L':
            cmd.create(o+'_CAP', "{} and resn CAP".format(o))
            cmd.show_as("stick", o+'_CAP')
            cmd.group(p, members=o+'_CAP', action='add', quiet=0)
        #.
        cmd.group(p, members=o, action='add', quiet=0)
    cmd.group(p, action='close')
#.

# Color objects: #Top_L: reds, Top_S: greens, Bottom_L: blues,
# Bottom_S: purples

colors = [(0.987,0.507,0.381), # Reds
          (0.957,0.309,0.222),
          (0.837,0.134,0.131),
          (0.666,0.063,0.086),
          (0.557,0.816,0.547), # Greens
          (0.339,0.712,0.406),
          (0.171,0.582,0.298),
          (0.018,0.443,0.185),
          (0.536,0.746,0.864), # Blues
          (0.326,0.619,0.803),
          (0.167,0.481,0.729),
          (0.044,0.334,0.624),
          (0.688,0.684,0.830), # Purples
          (0.551,0.538,0.752),
          (0.441,0.368,0.665),
          (0.341,0.174,0.571)]

for i,c in enumerate(colors):
    cmd.set_color('rubcol_{}'.format(i), c)
#.

# Color the indidual chains
i = 0
for p in topobj.keys():
    for u in ('L', 'S'):
        objnam = p + '_' + u

        for ch in chains[u]:
            col = 'rubcol_{}'.format(i)
            s = "/{}//{}".format(objnam, ch)
            cmd.color(col, s)
            #cmd.set('transparency', 0.5)
            #cmd.show_as("surface", s)
            i += 1
        #.
    #.
#.
python end


# Draw the symmetry axes

radius = 0.5

r, g, b = (0.0, 0.788, 0.0) # barium green
x1, y1, z1 = (0.550, 39.430, -2.055)
x2, y2, z2 = (0.563, 39.491, 100.700)
cmd.load_cgo( [ 9.0, x1, y1, z1, x2, y2, z2, radius, r, g, b, r, g, b ], "z" )

r, g, b = (0.922, 0.0, 0.149) # meitnerium red
x1, y1, z1 = -38.936, 78.311, 48.950
x2, y2, z2 =  39.918,  0.475, 51.786
cmd.load_cgo( [ 9.0, x1, y1, z1, x2, y2, z2, radius, r, g, b, r, g, b ], "xy" )

r, g, b = (0.0, 0.561, 1.0) # uranium blue
x1, y1, z1 = -38.918, 0.475, 48.640
x2, y2, z2 = 38.936, 78.391, 51.700
cmd.load_cgo( [ 9.0, x1, y1, z1, x2, y2, z2, radius, r, g, b, r, g, b ], "x-y" )

group Axes, z xy x-y

# We don't need these objects anymore
delete 8ruc_*

set_view (\
     1.0,    0.0,    0.0,\
     0.0,    1.0,    0.0,\
     0.0,    0.0,    1.0,\
     0.0,    0.0, -379.692321777,\
     0.0,   39.437149048,   50.325164795,\
   299.352325439,  460.032318115,  20.0 )

scene F1, store

turn x, 90
turn y, 45
scene F2, store

turn y, 90
scene F3, store

# Draw the active site

create sc, /Top_L//A/175+177+201+203+204+294+295+298+327+334 and (sidechain or name CA)
show sticks, sc
create mc, /Top_L//A/379+403-404
show sticks, mc
create CAP, /Top_L//A/477
show sticks, CAP
create Mg, /Top_L//A/476
show nb_spheres, Mg

group Active_site, sc mc CAP Mg
util.cbay('Active_site')

disable Bottom

# Create the objects displaying the H-bonds

distance Coord, /Mg//A/MG`476/MG, /sc//A/203/OD1
distance Coord, /Mg//A/MG`476/MG, /sc//A/204/OE1
distance Coord, /Mg//A/MG`476/MG, /CAP//A/477/O2
distance Coord, /Mg//A/MG`476/MG, /CAP//A/477/O6
distance Coord, /Mg//A/MG`476/MG, /sc//A/KCX`201/OQ2
distance Coord, /Mg//A/MG`476/MG, /CAP//A/CAP`477/O3
hide labels, Coord

distance Hbond, /CAP//A/CAP`477/O2, /sc//A/LYS`175/NZ
distance Hbond, /CAP//A/CAP`477/O7, /sc//A/LYS`334/NZ
distance Hbond, /CAP//A/CAP`477/O3P, /sc//A/LYS`334/NZ
distance Hbond, /CAP//A/CAP`477/O1P, /mc//A/GLY`404/N
distance Hbond, /CAP//A/CAP`477/O2P, /mc//A/GLY`403/N
distance Hbond, /CAP//A/CAP`477/O3, /sc//A/HIS`294/NE2
distance Hbond, /CAP//A/CAP`477/O3, /sc//A/KCX`201/OQ1
distance Hbond, /CAP//A/CAP`477/O5P, /sc//A/HIS`327/ND1
distance Hbond, /CAP//A/CAP`477/O4P, /sc//A/ARG`295/NH2
distance Hbond, /CAP//A/CAP`477/O6P, /sc//A/ARG`295/NE

distance Hbond, /CAP//A/CAP`477/O4, /mc//A/SER`379/O
distance Hbond, /CAP//A/CAP`477/O6, /sc//A/LYS`177/NZ
distance Hbond, /sc//A/ASP`203/OD2, /sc//A/LYS`177/NZ
distance Hbond, /sc//A/GLU`204/OE2, /sc//A/LYS`177/NZ

hide labels, Hbond

# Draw the octahedron of the Mg coordination
python
from pymol.cgo import *

V = ["/sc//A/204/OE1", "/CAP//A/477/O6", "/sc//A/203/OD1",
     "/sc//A/KCX`201/OQ2", "/CAP//A/CAP`477/O3", "/CAP//A/477/O2"]

# Pick off selections, create selection of each atom, and append to
# list of coordinates. The reason we do it this way is to make sure
# the atoms come in the right order.
coords = []
for s in V:
    cmd.select(s)
    c = cmd.get_model('(sele)', 1).get_coord_list()
    coords.append(c[0])
#.

# Create the CGO object that draws the outline of the
# coordination octahedron
octa = [
   LINEWIDTH, 2.0,
   BEGIN, LINES,
   COLOR, 0.8, 0.0, 0.8,
    # Top
    VERTEX,   coords[0][0], coords[0][1], coords[0][2],
    VERTEX,   coords[1][0], coords[1][1], coords[1][2],
    VERTEX,   coords[0][0], coords[0][1], coords[0][2],
    VERTEX,   coords[2][0], coords[2][1], coords[2][2],
    VERTEX,   coords[0][0], coords[0][1], coords[0][2],
    VERTEX,   coords[3][0], coords[3][1], coords[3][2],
    VERTEX,   coords[0][0], coords[0][1], coords[0][2],
    VERTEX,   coords[4][0], coords[4][1], coords[4][2],
    # Bottom
    VERTEX,   coords[5][0], coords[5][1], coords[5][2],
    VERTEX,   coords[1][0], coords[1][1], coords[1][2],
    VERTEX,   coords[5][0], coords[5][1], coords[5][2],
    VERTEX,   coords[2][0], coords[2][1], coords[2][2],
    VERTEX,   coords[5][0], coords[5][1], coords[5][2],
    VERTEX,   coords[3][0], coords[3][1], coords[3][2],
    VERTEX,   coords[5][0], coords[5][1], coords[5][2],
    VERTEX,   coords[4][0], coords[4][1], coords[4][2],
    # base
    VERTEX,   coords[1][0], coords[1][1], coords[1][2],
    VERTEX,   coords[2][0], coords[2][1], coords[2][2],
    VERTEX,   coords[2][0], coords[2][1], coords[2][2],
    VERTEX,   coords[3][0], coords[3][1], coords[3][2],
    VERTEX,   coords[3][0], coords[3][1], coords[3][2],
    VERTEX,   coords[4][0], coords[4][1], coords[4][2],
    VERTEX,   coords[4][0], coords[4][1], coords[4][2],
    VERTEX,   coords[1][0], coords[1][1], coords[1][2],
    END
   ]

cmd.load_cgo(octa, 'Poly', 1)
python end

group Active_site, Hbond Coord Poly, add

set_view (\
     0.408934534,    0.680647433,   -0.607859671,\
    -0.516867101,   -0.376204669,   -0.768969178,\
    -0.752076626,    0.628641784,    0.197961286,\
     0.000000000,    0.000000000,  -68.655250549,\
   -36.492912292,   37.300392151,   32.274772644,\
    54.128322601,   83.182174683,   20.0 )

scene F4, store

enable Bottom
disable Active_site
disable Top_L
disable Bottom_L
disable Top_S
disable Bottom_S

set_view (\
     0.866025388,    0.000000000,   -0.500000000,\
     0.000000000,    1.000000000,    0.000000000,\
     0.500000000,    0.000000000,    0.866025388,\
     0.000000000,    0.000000000, -379.692321777,\
     0.000000000,   39.437149048,   50.325164795,\
   299.352325439,  460.032318115,   20.000000000 )

scene F5, store

scene F1, recall
select none
