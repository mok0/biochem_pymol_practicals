# Methylmalonyl CaA mutase
# Copyright (C) 2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Version 1.0

reinitialize

cmd.set_color("greens_0", (0.883, 0.955, 0.862))
cmd.set_color("greens_1", (0.737, 0.896, 0.711))
cmd.set_color("greens_2", (0.557, 0.816, 0.547))
cmd.set_color("greens_3", (0.339, 0.712, 0.406))
cmd.set_color("greens_4", (0.171, 0.582, 0.298))
cmd.set_color("greens_5", (0.018, 0.443, 0.185))
cmd.set_color("blues_0", (0.858, 0.913, 0.965))
cmd.set_color("blues_1", (0.731, 0.839, 0.921))
cmd.set_color("blues_2", (0.536, 0.746, 0.864))
cmd.set_color("blues_3", (0.326, 0.619, 0.803))
cmd.set_color("blues_4", (0.167, 0.481, 0.729))
cmd.set_color("blues_5", (0.044, 0.334, 0.624))

# Change default values
set dash_color, white

# 4REQ Methylmalonyl-COA Mutase substrate complex, contains hetgroups:
# B12 COBALAMIN
# SCA SUCCINYL-COENZYME A
# MCA METHYLMALONYL-COENZYME A
# 5AD 5'-DEOXYADENOSINE
# GOL GLYCEROL
# GOL GLYCERIN; PROPANE-1,2,3-TRIOL

fetch 4req, async=0
remove /4req//C+D
color blues_2, /4req//A
color blues_5, /4req//B

# 2REQ Methylmalonyl-CoA Mutase, non-productive CoA complex, in open
# conformation representing substrate-free state, contains hetgroups:
# COA COENZYME A
# B12 COBALAMIN

fetch 2req, async=0
remove /2req//C+D
color greens_2, /2req//A
color greens_5, /2req//B
hide everything

# Fix secondary structure assignement
alter A/77-82/, ss='L'
alter A/235-236/, ss='L'

alter A/85-90/, ss='S'
alter A/116/, ss='S'
alter A/166-167/, ss='S'
alter A/196-199/, ss='S'
alter A/238-242/, ss='S'
alter A/265/, ss='S'
alter A/285/, ss='S'
alter A/365-366/, ss='S'

#   align mobile, target
align 2req, 4req

# Draw 4req substrate complex
# ===========================

g_s = ["4req_A", "4req_B", "4req_B12", "sCoA", "mCoA", "5dA", "4req_sc1"]

create 4req_A, 4req and chain A
create 4req_B, 4req and chain B
show cartoon, 4req_A
show cartoon, 4req_B

# B12
create 4req_B12, 4req and resnam B12
show sticks, 4req_B12
show sphere, /4req_B12//A/B12`800/CO

# SCA: succinyl-coenzyme A
create sCoA, 4req and resnam SCA
show sticks, sCoA

# MCA: methylmalonyl-coenzyme A
create mCoA, 4req and resnam MCA
show sticks, mCoA

# 5AD: 5'-deoxy-adenosine
create 5dA, 4req and resnam 5AD
show sticks, 5dA

# Create object of side chains binding CoA moity
python
sel = ["(/4req_B//B/45",]
for r in (75, 82, 87, 89, 114, 197, 207, 283, 285, 321):
    s = "/4req_A//A/{}".format(r)
    sel.append(s)
selstr = " or ".join(sel)
selstr = selstr + ") and (sidechain or name CA)"
cmd.create('4req_sc1', selstr)
cmd.show_as('sticks', '4req_sc1')
python end

for obj in g_s[2:]: util.cnc(obj)

# Create a group of all objects from 4req
cmd.group("Subst", members=" ".join(g_s))

for obj in ("4req_B12", "sCoA", "mCoA", "5dA"): util.cbaw(obj)
util.cnc('4req_sc1')

# H-bonds methylmalonyl-coenzyme A
dist hb4_m, /mCoA//A/MCA`802/O22`B, /4req_A//A/ARG`87/NH2
dist hb4_m, /mCoA//A/MCA`802/OP2`B, /4req_A//A/ARG`87/NE
dist hb4_m, /mCoA//A/MCA`802/OP1`B, /4req_A//A/SER`114/OG
dist hb4_m, /mCoA//A/MCA`802/OS1`B, /4req_A//A/GLN`197/NE2
dist hb4_m, /mCoA//A/MCA`802/OS4`B, /4req_B12//A/B12`800/O39
dist hb4_m, /mCoA//A/MCA`802/OP3`B, /4req_A//A/SER`285/OG
dist hb4_m, /mCoA//A/MCA`802/OS5`B, /4req_A//A/ARG`207/NE
dist hb4_m, /mCoA//A/MCA`802/OS4`B, /4req_A//A/ARG`207/NH2
dist hb4_m, /mCoA//A/MCA`802/OS4`B, /4req_A//A/TYR`89/OH
dist hb4_m, /mCoA//A/MCA`802/O33`B, /4req_A//A/ARG`283/NH2
dist hb4_m, /mCoA//A/MCA`802/O11`B, /4req_A//A/TYR`75/OH
dist hb4_m, /mCoA//A/MCA`802/N3`B, /4req_B//B/ARG`45/NH1
dist hb4_m, /mCoA//A/MCA`802/O4'`B, /4req_A//A/ARG`82/NH1
# '
hide labels, hb4_m
g_s.append('hb4_m')

# H-bonds succinyl-coenzyme A
dist hb4_s, /sCoA//A/SCA`801/O22`A, /4req_A//A/ARG`87/NH2
dist hb4_s, /sCoA//A/SCA`801/OP2`A, /4req_A//A/ARG`87/NE
#### 3.8A  dist hb4_s, /sCoA//A/SCA`801/OP1`A, /4req_A//A/SER`114/OG
dist hb4_s, /sCoA//A/SCA`801/OS1`A, /4req_A//A/GLN`197/NE2
dist hb4_s, /sCoA//A/SCA`801/OS4`A, /4req_B12//A/B12`800/O39
dist hb4_s, /sCoA//A/SCA`801/OP3`A, /4req_A//A/SER`285/OG
dist hb4_s, /sCoA//A/SCA`801/OS5`A, /4req_A//A/ARG`207/NE
dist hb4_s, /sCoA//A/SCA`801/OS4`A, /4req_A//A/ARG`207/NH2
dist hb4_s, /sCoA//A/SCA`801/OS4`A, /4req_A//A/TYR`89/OH
dist hb4_s, /sCoA//A/SCA`801/O33`A, /4req_A//A/ARG`283/NH2
dist hb4_s, /sCoA//A/SCA`801/O11`A, /4req_A//A/TYR`75/OH
dist hb4_s, /sCoA//A/SCA`801/N3`A, /4req_B//B/ARG`45/NH1
dist hb4_s, /sCoA//A/SCA`801/O4'`A, /4req_A//A/ARG`82/NH1
# '
hide labels, hb4_s
g_s.append('hb4_s')

cmd.group("Subst", members="hb4_m hb4_s", action='add', quiet=0)
for obj in g_s[2:]: cmd.disable(obj)


# Draw 2req, no substrate conformation
# ====================================

g_n = ["2req_A", "2req_B", "2req_B12", "CoA", "2req_sc1"]

create 2req_A, 2req and chain A
create 2req_B, 2req and chain B
show cartoon, 2req_A
show cartoon, 2req_B

# B12: cobalamin
create 2req_B12, 2req and resnam B12
show sticks, 2req_B12
show sphere, /2req_B12//A/B12`800/CO
util.cbaw('2req_B12')

# COA: coenzyme A
create CoA, 2req and resnam COA
show sticks, CoA

# Create object of side chains binding CoA moity
python
sel = ["(/2req_B//B/45",]
for r in (75, 82, 87, 89, 114, 197, 207, 283, 285, 321):
    s = "/2req_A//A/{}".format(r)
    sel.append(s)
selstr = " or ".join(sel)
selstr = selstr + ") and (sidechain or name CA)"
cmd.create('2req_sc1', selstr)
cmd.show_as('sticks', '2req_sc1')
python end

group Nosubst, 2req_B12 CoA 2req_sc1
for obj in g_n[2:]: util.cnc(obj); cmd.disable(obj)
cmd.group("Nosubst", members=" ".join(g_n))

for obj in ("2req_B12", "CoA"): util.cbaw(obj)
util.cnc('2req_sc1')

# H-bonds
dist hb2, /CoA//A/COA`801/O5A, /2req_A//A/LYS`321/NZ
dist hb2, /CoA//A/COA`801/N7A, /2req_sc1//A/ARG`82/NH2
# Out part of Arg sc is missing:
dist hb2, /CoA//A/COA`801/O4B, /2req_sc1//B/ARG`45/CG
hide labels, hb2
g_n.append('hb2')

cmd.group("Nosubst", members="hb2", action='add', quiet=0)
for obj in g_n[2:]: cmd.disable(obj)


set_view (\
     0.446881115,   -0.721168756,    0.529349685,\
     0.838183105,    0.544329107,    0.033973709,\
    -0.312641203,    0.428510606,    0.847720146,\
     0.000000000,    0.000000000, -299.786102295,\
    59.358108521,   60.733623505,    0.008686066,\
   236.353637695,  363.218566895,   20.0 )

disable Subst
enable Nosubst
for obj in g_n[2:]: cmd.disable(obj)
scene F1, store

disable Nosubst
# Only show secondary structure
for obj in g_s[2:]: cmd.disable(obj)
enable Subst

scene F2, store

set_view (\
     0.139795080,   -0.898762822,    0.415542156,\
     0.923590422,   -0.032927446,   -0.381933540,\
     0.356950760,    0.437190115,    0.825495064,\
    -0.001337431,    0.000646383,  -67.590240479,\
    69.281829834,   59.909652710,  -11.822284698,\
    58.211071014,   76.964744568,   20.0)

# F3 shows substrate bound structure with SCA
enable 4req_B12 sCoA hb4_s 4req_sc1
scene F3, store

enable 4req_B12 mCoA hb4_m 4req_sc1
disable sCoA hb4_s
scene F4, store

set_view (\
    -0.098580822,   -0.707934737,    0.699359179,\
     0.972587764,   -0.217234448,   -0.082806885,\
     0.210545883,    0.672035098,    0.709951282,\
    -0.001546036,    0.000619538, -108.864799500,\
    69.295585632,   58.490150452,  -12.022253036,\
    98.165672302,  119.529655457,   20.0)

disable Subst
enable Nosubst 2req_B12 CoA 2req_sc1 hb2
scene F5, store

set_view (\
    -0.759020329,   -0.506772339,    0.408726245,\
     0.234272450,   -0.798319817,   -0.554774284,\
     0.607447267,   -0.325332016,    0.724676549,\
    -0.000030994,    0.002531443,  -69.678932190,\
    69.259963989,   79.396888733,    0.283828735,\
    54.975154877,   84.483551025,   20.0 )

# H-bonds from B12 to enzyme
dist hb2_2, /2req_B12//A/B12`800/O44, /2req_A//A/GLY`609/N
dist hb2_2, /2req_B12//A/B12`800/O8R, /2req_A//A/THR`709/O
dist hb2_2, /2req_B12//A/B12`800/N3B, /2req_A//A/SER`655/OG
dist hb2_2, /2req_B12//A/B12`800/N52, /2req_A//A/ASP`611/OD1
dist hb2_2, /2req_B12//A/B12`800/O51, /2req_A//A/ASP`611/N
dist hb2_2, /2req_B12//A/B12`800/N29, /2req_A//A/ALA`373/O
dist hb2_2, /2req_B12//A/B12`800/CO, /2req_A//A/HIS`610/NE2
hide labels, hb2_2

# B12 binding site
# Binds Co: 610
create 2req_sc2, /2req_A//A/373+609+611+709 and backbone or (/2req_A//A/655+373+609+610+611+709 and (sidechain or name CA))
show sticks, 2req_sc2
util.cnc('2req_sc2')
cmd.group("Nosubst", members="hb2_2 2req_sc2", action='add', quiet=0)

scene F6, store

# H-bonds from B12 to enzyme
dist hb4_2, /4req_B12//A/B12`800/O44, /4req_A//A/GLY`609/N
dist hb4_2, /4req_B12//A/B12`800/O8R, /4req_A//A/THR`709/O
dist hb4_2, /4req_B12//A/B12`800/N3B, /4req_A//A/SER`655/OG
dist hb4_2, /4req_B12//A/B12`800/N52, /4req_A//A/ASP`611/OD1
dist hb4_2, /4req_B12//A/B12`800/O51, /4req_A//A/ASP`611/N
dist hb4_2, /4req_B12//A/B12`800/N29, /4req_A//A/ALA`373/O
dist hb4_2, /4req_B12//A/B12`800/CO, /4req_A//A/HIS`610/NE2
# Binding of 5dA
dist hb4_2, /5dA//A/5AD`803/N6, /4req_A//A/GLY`91/O
dist hb4_2, /5dA//A/5AD`803/O2', /4req_A//A/GLU`370/OE1
dist hb4_2, /5dA//A/5AD`803/O3', /4req_A//A/GLU`370/OE2
hide labels, hb4_2

# B12 binding site
# Binds Co: 610
create 4req_sc2, /4req_A//A/91+373+609+611+709 and backbone or (/4req_A//A/370+655+373+609+610+611+709 and (sidechain or name CA))
show sticks, 4req_sc2
util.cnc('4req_sc2')
cmd.group("Subst", members="hb4_2 4req_sc2", action='add', quiet=0)

disable Nosubst
enable Subst 5dA

set_view (\
    -0.759020329,   -0.506772339,    0.408726245,\
     0.234272450,   -0.798319817,   -0.554774284,\
     0.607447267,   -0.325332016,    0.724676549,\
    -0.000030994,    0.002531443,  -69.678932190,\
    69.259963989,   79.396888733,    0.283828735,\
    54.975154877,   84.483551025,   20.0 )

scene F7, store

# Illustrate domain A TIM-barrel #

color grey30, A/4-64/
color grey30, A/404-728/
hide cartoon, A/4-64/
hide cartoon, A/404-728/
show ribbon, A/4-64/
show ribbon, A/404-728/

color nickel, A/65-403/
color meitnerium, A/65-403/ and ss 'H'
# color yelloworange, A/65-403/ and SS 'S'
color gold, A/65-403/ and ss 'S'

set_view (\
     0.615416586,   -0.785446048,   -0.065723374,\
     0.539479792,    0.358968109,    0.761637866,\
    -0.574637055,   -0.504178703,    0.644659698,\
    -0.001238914,   -0.000738196,  -98.963233948,\
    67.457962036,   61.262233734,   -6.645058632,\
    80.986328125,  116.966964722,   20.)

disable Nosubst
disable 4req_B sCoA 4req_sc* hb*
scene F8, store

delete 2req 4req

scene F1, recall
