# Copyright (C) 2015-2017 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Allosterical changes of ATCase
# Version 2.3

python
def load_from_local_or_repo(fname, objnam):
    import posixpath
    bb = "https://bitbucket.org/mok0/biochem_pymol_practicals/raw/master/"
    localpath = os.path.join("pdb", fname)
    repopath = posixpath.join(bb, "atcase", "pdb", fname)
    if os.path.exists(fname):
        cmd.load(fname, objnam)
    elif os.path.exists(localpath):
        cmd.load(localpath, objnam)
    else:
        cmd.load(repopath, objnam)
python end


reinitialize
Tcol = "limegreen"
Rcol = "teal"

# Draw 3-fold axis
x1, y1, z1 = 61.0, 35.218, -50.0
x2, y2, z2 = 61.0, 35.218, 120.0
r1,g1,b1 = 1,0,0
r2,g2,b2 = 1,1,0
radius = 0.5
cmd.load_cgo( [ 9.0, x1, y1, z1, x2, y2, z2, radius, r1, g1, b1, r2, g2, b2 ], "3-fold axis" )

# T-state structure

load_from_local_or_repo("4at1.pdb1.gz", "4at1")
split_states 4at1
delete 4at1

# The chain names are identical (A, B, C, D) in all states, therefore we
# need to alter them, otherwise PyMol becomes confused.
alter /4at1_0002//A, chain="E"
alter /4at1_0002//B, chain="F"
alter /4at1_0002//C, chain="G"
alter /4at1_0002//D, chain="H"

alter /4at1_0003//A, chain="I"
alter /4at1_0003//B, chain="J"
alter /4at1_0003//C, chain="K"
alter /4at1_0003//D, chain="L"

create T_trimer1, /4at1_0001//C or /4at1_0002//G or /4at1_0003//K
create T_trimer2, /4at1_0001//A or /4at1_0002//E or /4at1_0003//I

# Loop over trimers
python
for i in (1,2):
    ob = "T_trimer{}".format(i)
    cmd.hide("everything", ob)
    cmd.color(Tcol, ob)
    cmd.show("cartoon", ob)
python end

create T_dimer1, /4at1_0001//B+D
create T_dimer2, /4at1_0002//F+H
create T_dimer3, /4at1_0003//L+J

# Loop over dimers
python
for i in (1,2,3):
    ob = "T_dimer{}".format(i)
    cmd.hide("everything", ob)
    cmd.color(Tcol, ob)
    cmd.show("cartoon", ob)
    cmd.delete("4at1_{:04d}".format(i))
python end

# R-state structure

load_from_local_or_repo("8atc.pdb1.gz", "8atc")
split_states 8atc
delete 8atc

# The chain names are identical (A, B, C, D) in all states, therefore we
# need to alter them, otherwise PyMol becomes confused.
alter /8atc_0002//A, chain="E"
alter /8atc_0002//B, chain="F"
alter /8atc_0002//C, chain="G"
alter /8atc_0002//D, chain="H"

alter /8atc_0003//A, chain="I"
alter /8atc_0003//B, chain="J"
alter /8atc_0003//C, chain="K"
alter /8atc_0003//D, chain="L"

create R_trimer2, /8atc_0001//C or /8atc_0002//G or /8atc_0003//K
create R_trimer1, /8atc_0001//A or /8atc_0002//E or /8atc_0003//I

# Loop over trimers
python
for i in (1,2):
    ob = "R_trimer{}".format(i)
    cmd.hide("everything", ob)
    cmd.color(Rcol, ob)
    cmd.show("cartoon", ob)
python end

create R_dimer1, /8atc_0001//B+D
create R_dimer2, /8atc_0002//F+H
create R_dimer3, /8atc_0003//L+J

# Loop over dimers
python
for i in (1,2,3):
    ob = "R_dimer{}".format(i)
    cmd.hide("everything", ob)
    cmd.color(Rcol, ob)
    cmd.show("cartoon", ob)
    cmd.delete("8atc_{:04d}".format(i))
python end

### Now create cofactor objects

create ATP, resn ATP
create PALA, resn PAL

util.cbaw('ATP')
util.cbaw('PALA')
show sticks, ATP
show sticks, PALA

delete 4at1
delete 8atc

# Hide R structure for view 1
disable R_*
disable PALA

set_view (\
    -0.921818554,   -0.387524426,    0.008638745,\
     0.387603104,   -0.921757519,    0.011211118,\
     0.003618889,    0.013684572,    0.999899268,\
     0.000058770,    0.000065595, -430.355743408,\
    54.857185364,   24.353317261,   34.773944855,\
   391.037506104,  469.662811279,  -20.0 )

scene F1, store

disable T_*
disable ATP
enable R_*
enable PALA
scene F2, store

set_view (\
    -0.902373970,   -0.042430051,    0.428859234,\
     0.430801213,   -0.062477689,    0.900280535,\
    -0.011404329,    0.997143924,    0.074655019,\
     0.000058770,    0.000065595, -430.355743408,\
    54.857185364,   24.353317261,   34.773944855,\
   340.197052002,  520.503295898,  -20.0)
scene F6, store

disable R_*
disable PALA
enable T_*
enable ATP
scene F5, store

# Scene 3
disable all

enable T_trimer*
enable 3-fold_axis

hide cartoon, T_trimer1
hide cartoon, T_trimer2
show ribbon, T_trimer1
show ribbon, T_trimer2
cmd.color(Tcol, 'T_trimer1')
cmd.color('gray80', 'T_trimer2')

set_view (\
    -0.983686209,   -0.179739833,    0.007400273,\
     0.179706901,   -0.983707547,   -0.004845673,\
     0.008151649,   -0.003435359,    0.999960542,\
     0.000024408,    0.000086859, -324.364685059,\
    56.876373291,   38.015319824,   34.415542603,\
   285.040710449,  363.666015625,  -20.0 )

scene F3, store

disable T_trimer*
enable R_trimer*

hide cartoon, R_trimer1
hide cartoon, R_trimer2
show ribbon, R_trimer1
show ribbon, R_trimer2
cmd.color(Rcol, 'R_trimer1')
cmd.color('gray80', 'R_trimer2')

scene F4, store

# Scene F7

enable T_trimer*
enable R_trimer*
cmd.color(Tcol, 'T_trimer*')
cmd.color(Rcol, 'R_trimer*')

set_view (\
    -0.397008628,    0.917725205,    0.012949464,\
    -0.917718112,   -0.397133380,    0.009104967,\
     0.013499880,   -0.008271109,    0.999873817,\
    -0.000197530,    0.000056341, -312.843292236,\
    59.297412872,   34.555248260,   34.158821106,\
   289.709991455,  335.957550049,  -20.0 )

scene F7, store

# Reset to view F1
scene F1, recall
