# Copyright (C) 2015-2017 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Illustrate R to T transition of ATCase
# Version 2.3

python
def load_from_local_or_repo(fname, objnam):
    import posixpath
    bb = "https://bitbucket.org/mok0/biochem_pymol_practicals/raw/master/"
    localpath = os.path.join("pdb", fname)
    repopath =  posixpath.join(bb, "atcase", "pdb", fname)
    if os.path.exists(fname):
        cmd.load(fname, objnam)
    elif os.path.exists(localpath):
        cmd.load(localpath, objnam)
    else:
        cmd.load(repopath, objnam)
python end

reinitialize

# Settings
set dash_color, white

# Draw 3-fold axis
x1, y1, z1 = 61.0, 35.218, -50.0
x2, y2, z2 = 61.0, 35.218, 120.0
r1,g1,b1 = 1,0,0
r2,g2,b2 = 1,1,0
radius = 0.5
cmd.load_cgo( [ 9.0, x1, y1, z1, x2, y2, z2, radius, r1, g1, b1, r2, g2, b2 ], "3-fold axis" )


# T-state structure
load_from_local_or_repo("4at1.pdb1.gz", "4at1")
split_states 4at1
delete 4at1

# The chain names are identical (A, B, C, D) in all states, therefore we
# need to alter them, otherwise PyMol becomes confused.
alter /4at1_0002//A, chain="E"
alter /4at1_0002//B, chain="F"
alter /4at1_0002//C, chain="G"
alter /4at1_0002//D, chain="H"

alter /4at1_0003//A, chain="I"
alter /4at1_0003//B, chain="J"
alter /4at1_0003//C, chain="K"
alter /4at1_0003//D, chain="L"

create DodecamerT, 4at1_00*

hide everything, DodecamerT
show ribbon, DodecamerT
color gray60, DodecamerT

python
for i in (1,2,3):
    ob = "4at1_{:04d}".format(i)
    cmd.hide("everything", ob)
    cmd.disable(ob)
python end


# R-state structure #

load_from_local_or_repo("8atc.pdb1.gz", "8atc")
split_states 8atc
delete 8atc

# The chain names are identical (A, B, C, D) in all states, therefore we
# need to alter them, otherwise PyMol becomes confused.
alter /8atc_0002//A, chain="E"
alter /8atc_0002//B, chain="F"
alter /8atc_0002//C, chain="G"
alter /8atc_0002//D, chain="H"

alter /8atc_0003//A, chain="I"
alter /8atc_0003//B, chain="J"
alter /8atc_0003//C, chain="K"
alter /8atc_0003//D, chain="L"

create DodecamerR, 8atc_00*
hide everything, DodecamerR
show ribbon, DodecamerR
color gray60, DodecamerR

## Switch dodecamers off
disable Dodecamer*

# T-state strucure, details #

create TrimerT, /4at1_0001//A or /4at1_0002//E or /4at1_0003//I
hide everything, TrimerT
color tv_blue, TrimerT
show ribbon, TrimerT
color purpleblue, /TrimerT//A
color marine, /TrimerT//E
color lightblue, /TrimerT//I
delete 4at1

create Tsite, /TrimerT//E/52-55+105+134+167+229+231+267 or /TrimerT//I/80+84
util.cbab('Tsite')
show sticks, Tsite

## Delete unwanted objects
python
for i in (1,2,3):
    ob = "4at1_{:04d}".format(i)
    cmd.hide("everything", ob)
    cmd.disable(ob)
    cmd.delete(ob)
python end


# R-state structure, details #

create TrimerR, /8atc_0001//C or /8atc_0002//G or /8atc_0003//K
hide everything, TrimerR

color raspberry, TrimerR
show ribbon, TrimerR
color limegreen, /TrimerR//G
color limon, /TrimerR//K
color chartreuse, /TrimerR//C

## Delete unwanted objects
python
for i in (1,2,3):
    ob = "8atc_{:04d}".format(i)
    cmd.hide("everything", ob)
    cmd.disable(ob)
    cmd.delete(ob)
python end

create Rsite, /TrimerR//G/52-55+105+134+167+229+231+267 or /TrimerR//K/80+84
util.cbag('Rsite')
show sticks, Rsite

# PALA
create PALA, /TrimerR//C+G+K/ and resn PAL
util.cbaw('PALA')
show sticks, PALA

## Show a few water molecules
create H2O, /TrimerR//G/329+331
show nb_spheres, H2O
color red, H2O

## H-bonds in R-state structure
dist Hbonds, /PALA//G/311/O2, /H2O//G/329/O
dist Hbonds, /PALA//G/311/O3, /Rsite//G/167/NH2
dist Hbonds, /PALA//G/311/O2, /Rsite//G/167/NE
dist Hbonds, /PALA//G/311/O3, /Rsite//K/84/NZ
dist Hbonds, /PALA//G/311/O4, /Rsite//K/84/NZ
dist Hbonds, /PALA//G/311/O5, /Rsite//G/231/OE1
dist Hbonds, /PALA//G/311/O5, /Rsite//G/229/NH1
dist Hbonds, /PALA//G/311/O4, /Rsite//G/229/NH1
dist Hbonds, /PALA//G/311/O5, /H2O//G/331/O
dist Hbonds, /PALA//G/311/O1, /Rsite//G/HIS`134/NE2
dist Hbonds, /PALA//G/311/O1, /Rsite//G/55/OG1
dist Hbonds, /PALA//G/311/O1, /Rsite//G/105/NH2
dist Hbonds, /PALA//G/311/O3P, /Rsite//G/55/OG1
dist Hbonds, /PALA//G/311/O1P, /Rsite//K/80/OG
dist Hbonds, /PALA//G/311/O1P, /Rsite//K/84/NZ
dist Hbonds, /PALA//G/311/O1P, /Rsite//G/105/NH1
dist Hbonds, /PALA//G/311/O2P, /Rsite//G/54/NH1
dist Hbonds, /Rsite//G/229/N, /H2O//G/HOH`331/O
dist Hbonds, /PALA//G/311/O2P, /Rsite//G/54/N
dist Hbonds, /PALA//G/311/O2P, /Rsite//G/53/N
dist Hbonds, /PALA//G/311/O3P, /Rsite//G/55/N
dist Hbonds, /PALA//G/PAL`311/O3P,  /Rsite//G/105/NH1
dist Hbonds, /PALA//G/PAL`311/O3P, /Rsite//G/52/OG
dist Hbonds, /PALA//G/311/N2, /Rsite//G/267/O

hide labels, Hbonds


# Scene F1 #

disable TrimerR
disable Rsite
disable PALA
disable Hbonds
disable H2O

set_view (\
    -0.624814928,   -0.371873826,   -0.686526597,\
     0.571055710,   -0.817286909,   -0.077024408,\
    -0.532444358,   -0.440169573,    0.723012030,\
    -0.000018815,   -0.000114389,  -71.915328979,\
    77.418609619,   22.401706696,   18.120145798,\
    61.610538483,   82.192863464,  -20.000005722 )
scene F1, store

# Scene F2 #

disable TrimerT
disable Tsite
enable TrimerR
enable Rsite
enable PALA
enable Hbonds
enable H2O

scene F2, store

# Scene F3 #

disable TrimerR
disable Rsite
disable PALA
disable H2O
disable Hbonds
enable TrimerT
enable Tsite

color red, /TrimerT//A/232-240
color red, /TrimerT//E/232-240
color red, /TrimerT//I/232-240

color red, /TrimerT//A/75-79
color red, /TrimerT//E/75-79
color red, /TrimerT//I/75-79

set_view (\
    -0.991492867,   -0.014699643,   -0.129338518,\
     0.129885375,   -0.046166781,   -0.990451396,\
     0.008588186,   -0.998820662,    0.047682688,\
    -0.000209865,    0.000168100, -252.318618774,\
    58.982875824,   35.763710022,   17.832733154,\
   208.056579590,  296.557098389,  -20.0 )

scene F3, store

# Scene F4 #

disable TrimerT
disable Tsite
enable TrimerR

color red, /TrimerR//C/232-240
color red, /TrimerR//G/232-240
color red, /TrimerR//K/232-240

color red, /TrimerR//C/75-79
color red, /TrimerR//G/75-79
color red, /TrimerR//K/75-79

scene F4, store

scene F1, recall
