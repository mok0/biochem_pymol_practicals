# Copyright (C) 2015-2017 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Show binding of PALA to ATCase.
# Version 2.3

python
def load_from_local_or_repo(fname, objnam):
    import posixpath
    bb = "https://bitbucket.org/mok0/biochem_pymol_practicals/raw/master/"
    localpath = os.path.join("pdb", fname)
    repopath =  posixpath.join(bb, "atcase", "pdb", fname)
    if os.path.exists(fname):
        cmd.load(fname, objnam)
    elif os.path.exists(localpath):
        cmd.load(localpath, objnam)
    else:
        cmd.load(repopath, objnam)
python end

reinitialize

# Settings
set dash_color, white

load_from_local_or_repo("8atc.pdb1.gz", "8atc")
split_states 8atc
hide everything

cartoon loop

create PALA,  8atc_0001 and chain A and resn PAL
show sticks, PALA
util.cbaw('PALA')

create Subunit1, /8atc_0003//A
color marine, Subunit1

create Subunit2, /8atc_0001//A
color brightorange, Subunit2

show cartoon, Subunit1
show cartoon, Subunit2

create Subunit1_SC, /8atc_0003//A/80+84 and (sidechain or name ca)
create Subunit2_SC, /8atc_0001//A/54+55+105+134+167+229 and (sidechain or name ca)
show sticks, Subunit1_SC
show sticks, Subunit2_SC
util.cbab('Subunit1_SC')
util.cbao('Subunit2_SC')

create H2O, /Subunit2//A/330+337
show nb_spheres, H2O
color oxygen, H2O

dist Hbonds, /PALA//A/311/O3, /H2O//A/330/O
dist Hbonds, /PALA//A/311/O3, /Subunit2_SC//A/167/NE
dist Hbonds, /PALA//A/311/O2, /Subunit2_SC//A/167/NH2
dist Hbonds, /PALA//A/311/O2, /Subunit1_SC//A/84/NZ
dist Hbonds, /PALA//A/311/O5, /Subunit1_SC//A/84/NZ
dist Hbonds, /PALA//A/311/O5, /Subunit2_SC//A/229/NH1
dist Hbonds, /PALA//A/311/O4, /Subunit2_SC//A/229/NH1
dist Hbonds, /PALA//A/311/O4, /H2O//A/337/O

dist Hbonds, /PALA//A/311/O1, /Subunit2_SC//A/HIS`134/NE2
dist Hbonds, /PALA//A/311/O1, /Subunit2_SC//A/55/OG1
dist Hbonds, /PALA//A/311/O1, /Subunit2_SC//A/105/NH2

dist Hbonds, /PALA//A/311/O3P, /Subunit2_SC//A/55/OG1

dist Hbonds, /PALA//A/311/O1P, /Subunit1_SC//A/80/OG
dist Hbonds, /PALA//A/311/O1P, /Subunit1_SC//A/84/NZ
dist Hbonds, /PALA//A/311/O1P, /Subunit2_SC//A/105/NH1

dist Hbonds, /PALA//A/311/O2P, /Subunit2_SC//A/54/NH1
hide labels, Hbonds

# Delete objects we don't need anymore
delete 8atc
for i in (1,2,3): cmd.delete("8atc_{:04d}".format(i))

set_view (\
     0.023020696,    0.699718535,   -0.714046538,\
     0.852622986,   -0.386683881,   -0.351430863,\
    -0.522015512,   -0.600720704,   -0.605496168,\
     0.000000000,    0.000000000,  -47.926670074,\
    68.642997742,   53.131000519,   63.053001404,\
    40.836025238,   55.017322540,  -20.000000000 )

scene F2, store


set_view (\
     0.776151896,    0.237798169,   -0.583985627,\
     0.339147121,   -0.938217998,    0.068711683,\
    -0.531566739,   -0.251384228,   -0.808847010,\
    -0.000010835,    0.000732645, -263.451019287,\
    72.531753540,   38.072647095,   62.035266876,\
   226.894668579,  300.067108154,  -20.000000000 )

scene F1, store
