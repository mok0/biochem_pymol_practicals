# Copyright (C) 2015-2017 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Examine the various ATCase ligands.
# Version 2.3

python
def load_from_local_or_repo(fname, objnam):
    import posixpath
    bb = "https://bitbucket.org/mok0/biochem_pymol_practicals/raw/master/"
    localpath = os.path.join("pdb", fname)
    repopath =  posixpath.join(bb, "atcase", "pdb", fname)
    if os.path.exists(fname):
        cmd.load(fname, objnam)
    elif os.path.exists(localpath):
        cmd.load(localpath, objnam)
    else:
        cmd.load(repopath, objnam)
python end

reinitialize

set all_states, on

# ATP-bound structure
load_from_local_or_repo("4at1.pdb1.gz", "4at1")
hide everything, 4at1

# CTP-bound structure

load http://www.rcsb.org/pdb/files/1RAI.pdb1.gz, 1rai

hide everything, 1rai

cartoon loop

set all_states, off

create SubunitA, /4at1//B
color limegreen, SubunitB
show cartoon, SubunitA

set all_states, off

create Ligand1, /SubunitA//B/ATP/
show sticks, Ligand1
util.cbaw('Ligand1')
#

create Ligand1_sc, /SubunitA//B/9+11-12+19+60+89+94
show sticks, Ligand1_sc
util.cbag('Ligand1_sc')

distance Ligand1_hbond, /Ligand1//B/155/N6, /Ligand1_sc//B/89/O
distance Ligand1_hbond, /Ligand1//B/155/N1, /Ligand1_sc//B/11/N
distance Ligand1_hbond, /Ligand1//B/155/N1, /Ligand1_sc//B/12/N
distance Ligand1_hbond, /Ligand1//B/155/N3, /Ligand1_SC//B/60/NZ
distance Ligand1_hbond, /Ligand1//B/155/O2', /Ligand1_SC//B/60/NZ
distance Ligand1_hbond, /Ligand1//B/155/O2', /Ligand1_SC//B/9/O
distance Ligand1_hbond, /Ligand1//B/155/O3', /Ligand1_SC//B/19/OD1
distance Ligand1_hbond, /Ligand1//B/155/O1B, /Ligand1_SC//B/94/NZ
distance Ligand1_hbond, /Ligand1//B/155/O2B, /Ligand1_SC//B/94/NZ
hide labels, Ligand1_hbond

################
# There is something odd with the ribose of residue 155.
# The result is that PyMOL forms bonds between atoms in the ring
# that should not be connected. To fix this, we move atom
# /Ligand1//B/ATP`155/O2' about 0.5 Å away.
# The original position of this atom from the PDB file:
# HETATM 7132  O2' ATP B 155      14.550  82.961  29.641
# A better position is: [[15.05, 83.6, 29.46]]

translate [15.05-14.55,83.6-82.961,29.46-29.641], /Ligand1//B/155/O2'

# Finally, remove the spurious bonds:
unbond  /Ligand1//B/155/O2', /Ligand1//B/155/O3'
unbond  /Ligand1//B/155/C3', /Ligand1//B/155/O2'
################

create Ligand2, /4at1//B/109+114+138+141
show sticks, Ligand2

create Zn, /4at1//B/154
show nb_spheres, Zn

dist Ligand2_hbond, /Ligand2//B/109/SG, /Zn//B/154/ZN
dist Ligand2_hbond, /Ligand2//B/114/SG, /Zn//B/154/ZN
dist Ligand2_hbond, /Ligand2//B/138/SG, /Zn//B/154/ZN
dist Ligand2_hbond, /Ligand2//B/141/SG, /Zn//B/154/ZN
hide labels, Ligand2_hbond

set_view (\
     0.354338735,   -0.703177929,   -0.616430283,\
     0.436278492,    0.707373261,   -0.556132615,\
     0.827107310,   -0.071877241,    0.557433307,\
    -0.000077911,   -0.000032194, -168.562759399,\
    20.652767181,   62.361660004,   28.523092270,\
   132.341674805,  204.790100098,  -20.0 )

scene F1, store

### Scene F2 ###

set_view (\
     0.803516328,   -0.292267025,   -0.518597364,\
     0.578835964,    0.180247322,    0.795272648,\
    -0.138957754,   -0.939199984,    0.314005286,\
    -0.000240443,    0.000055786,  -63.159679413,\
    17.408718109,   78.788551331,   24.549758911,\
    26.935794830,   99.384208679,  -19.999994278 )
disable Ligand2*
disable Zn

scene F2, store

disable SubunitA
disable Ligand1*

## Subunit B ##

create SubunitB, /1rai//B
show cartoon, SubunitB
color cyan, SubunitB

create Ligand3, /SubunitB//B/CTP/
show sticks, Ligand3
util.cbaw('Ligand3')

create Ligand3_sc, /1rai//B/12+20+60+94+
show sticks, Ligand3_sc
util.cbag('Ligand1_sc')

dist Ligand3_hbond, /Ligand3//B/999/N4, /Ligand3_SC//B/12/O
dist Ligand3_hbond, /Ligand3//B/999/O2, /Ligand3_SC//B/60/NZ
dist Ligand3_hbond, /Ligand3//B/999/O2A, /Ligand3_SC//B/94/NZ
dist Ligand3_hbond, /Ligand3//B/999/O1G, /Ligand3_SC//B/20/NE2
hide labels, Ligand3_hbond

set_view (\
     0.580369830,   -0.239953175,   -0.778199017,\
     0.812197804,    0.101109318,    0.574552715,\
    -0.059184127,   -0.965508342,    0.253569514,\
    -0.000249330,    0.000024100,  -66.221534729,\
    15.492635727,   80.628005981,   24.948419571,\
    56.086662292,   76.359542847,  -20)
scene F3, store


set_view (\
     0.310093969,   -0.863430381,   -0.397909373,\
     0.515282512,    0.504375279,   -0.692884743,\
     0.798953414,    0.009823064,    0.601316690,\
     0.000000000,    0.000000000,  -40.023548126,\
    34.722999573,   49.562999725,   23.895000458,\
     3.799340010,   76.247749329,  -19.999992371 )

disable SubunitB
disable Ligand3*
enable SubunitA
enable Ligand2*
enable Zn

scene F4, store

scene F1, recall

delete 4at1
delete 1rai
