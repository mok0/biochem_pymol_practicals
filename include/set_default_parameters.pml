# Set default graphics parameters

# Set cartoon parameters
set cartoon_ladder_mode, 1
set cartoon_ring_finder, 1
set cartoon_ring_mode, 0
#set cartoon_transparency, 0.4, 23sb*
#set cartoon_oval_length, 0.6, 16s*
#set cartoon_oval_width, 0.2, 16s*

# Set radius of dashed lines (i.e. Hbonds)
set dash_radius, 0.1

# Default dash-color
set dash_color, aquamarine

# Set transparent background for render
set opaque_background, off

# Turn off shadows
set ray_shadows, 0

# Less light

set specular, 0.1

set mesh_radius, 0.04
set stick_radius, 0.15

#set sphere_scale, 0.15

# Christian Folsted's default settings
from pymol import cmd
cmd.set("ray_shadows","0")
cmd.set("ray_trace_fog","1")
cmd.set("stick_radius","0.25")
cmd.set("stick_ball","off")
cmd.set("stick_ball_ratio","1.5")
cmd.set("cartoon_loop_quality","20")
cmd.set("cartoon_oval_quality","20")
cmd.set("cartoon_oval_width","0.20")
cmd.set("cartoon_oval_length","1.0")
cmd.set("cartoon_dumbbell_length","1.2")
cmd.set("cartoon_dumbbell_width","0.24")
cmd.set("cartoon_dumbbell_radius","0.2")
#cmd.set("cartoon_loop_radius","0.40")
cmd.set("cartoon_loop_radius","0.25")
cmd.set("cartoon_rect_length","1.6")
cmd.set("cartoon_ladder_radius","0.35")
cmd.set("spec_reflect","0.15")
cmd.set("ray_trace_mode","0")
cmd.set("antialias","2")
cmd.set("ribbon_smooth","1")
cmd.set("ribbon_radius","0.4")

cmd.set_color("ca",[0.8,0.8,0.8])
cmd.set_color("ni",[0.13,0.35,0.68])
cmd.set_color("ox",[0.75,0.18,0.19])
cmd.set_color("su", [1,0.9,0.5])
cmd.set_color("ph",[1,0.9,0.6])
cmd.set_color("mg",[0.7,0.7,0.8])
cmd.set_color("fe",[0.4,0.15,0.10])
