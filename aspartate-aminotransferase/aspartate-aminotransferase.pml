# Copyright (C) 2016 Morten Kjeldgaard <mok@mbg.au.dk>
# License: Creative Commons cc-by-sa version 4.0
# Aspartate Aminotransferase Reconstituted with 1-Deazapyridoxal
# 5'-Phosphate: Internal Aldimine and Stable l-Aspartate External
# Aldimine*
# Version 1.0

reinitialize

# Change default values
set dash_color, white

# Contains Asp-PLP external aldimine
fetch 3qpg, async=0

# L-peptide linking
fetch 3qn6, async=0
alter /3qn6//A/3QN`258/C8, name='CA'
alter /3qn6//A/3QN`258/C1, name='C'
alter /3qn6//A/3QN`258/N1, name='N'
alter /3qn6//A/3QN`258/O1, name='O'

# Residue 157 contains an alternate position that destroys the display
# of the beta-strand. Just get rid of it.
remove 3qn6 and alt D
alter 3qn6, alt=''

hide everything

# align mobile, target
align 3qn6, 3qpg

color erbium, 3qpg
show cartoon, 3qpg

color molybdenum, 3qn6
show cartoon, 3qn6

create PLP, 3qpg and resn 3QP
cmd.show_as('sticks', 'PLP')
util.cbaw('PLP')

create PLP_int, 3qn6 and resn 3QN and not (name N or name O or name C)
cmd.show_as('sticks', 'PLP_int')
util.cnc('PLP_int')

create 3qpg_sc, /3qpg//A/142+195+226+255+257+258+266+386 and not (name C or name N or name O)
create 3qn6_sc, /3qn6//A/142+195+226+255+257+266+386 and not (name C or name N or name O)
show sticks, 3qpg_sc 3qn6_sc
util.cnc('3qpg_sc 3qn6_sc')

dist Hbond, /PLP//A/422/O04, /3qpg//A/195/ND2
dist Hbond, /PLP//A/422/O04, /3qpg//A/226/OH
dist Hbond, /PLP//A/422/O10, /3qpg//A/266/NH1
dist Hbond, /PLP//A/422/O10, /3qpg//A/257/OG
dist Hbond, /PLP//A/422/O11, /3qpg//A/266/NH2
dist Hbond, /PLP//A/422/O12, /3qpg//A/255/OG
dist Hbond, /PLP//A/422/O23, /3qpg//A/386/NH2
dist Hbond, /PLP//A/422/O24, /3qpg//A/386/NH1
dist Hbond, /PLP//A/422/O21, /3qpg//A/142/NE1

dist Hbond_2, /PLP_int//A/258/O2, /3qn6//A/195/ND2
dist Hbond_2, /PLP_int//A/258/O2, /3qn6//A/226/OH
dist Hbond_2, /PLP_int//A/258/O5, /3qn6//A/266/NH1
dist Hbond_2, /PLP_int//A/258/O3, /3qn6//A/266/NH2
dist Hbond_2, /PLP_int//A/258/O4, /3qn6//A/255/OG
dist Hbond_2, /PLP_int//A/258/O5, /3qn6//A/257/OG
dist Hbond_2, /PLP_int//A/258/N2, /3qn6//A/226/OH

hide labels, Hbond Hbond_2

group aldimine_ext, 3qpg 3qpg_sc PLP Hbond
group aldimine_int, 3qn6 3qn6_sc PLP_int Hbond_2

# Scene F1, view of AAT with PLP in external complex

disable aldimine_int
disable 3qpg_sc Hbond

set_view (\
     0.817245305,    0.220950291,    0.532244742,\
    -0.563492596,    0.112854138,    0.818376541,\
     0.120754585,   -0.968732297,    0.216733292,\
     0.000000000,    0.000000000, -193.289596558,\
   -38.924163818,   23.646520615,   -2.672963142,\
   152.390991211,  234.188201904,  20.0 )

scene F1, store

# Scene F2, view of AAT with PLP covalently attached to Lys-258

enable  aldimine_int
disable 3qn6_sc Hbond_2
disable aldimine_ext
scene F2, store

set_view (\
     0.821632266,   -0.365182459,    0.437665373,\
    -0.530678511,   -0.770343065,    0.353482634,\
     0.208066851,   -0.522695899,   -0.826736093,\
     0.000000000,    0.000000000,  -51.506301880,\
   -38.924163818,   23.646520615,   -2.672963142,\
    32.715431213,   70.297157288,   20.0 )

# View F3, details of PLP site

disable aldimine_int
enable aldimine_ext 3qpg_sc Hbond
scene F3, store

# View F3, details of internally attached PLP

disable aldimine_ext
enable aldimine_int 3qn6_sc Hbond_2
scene F4, store


scene F1, recall
